//
//  KanaTextField.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

protocol KanaTextFieldDelegate{
    func didValueChanged(sender:UITextField)
}

class KanaTextField: UITextField {
    
    var kanaTextFieldDegate:KanaTextFieldDelegate?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupViews()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews(){
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 4
        self.layer.borderColor = Color.BORDER_COLOR
        self.addTarget(self, action: #selector(didValueChanged), for: .editingChanged)
        
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 8, height: self.frame.height))
        self.leftView = leftView
        self.leftViewMode = .always
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self.text)
    }
    
    func removeBorder(){
        self.layer.borderWidth = 0
    }

    @objc private func didValueChanged(_ sender:UITextField){
        kanaTextFieldDegate?.didValueChanged(sender: sender)
    }
}
