//
//  CatalogDetailViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 15/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import WebKit

class CatalogDetailViewController: KanaViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var loadingStackView: UIStackView!
    @IBOutlet var webViewContainer: UIView!
    var webView: WKWebView!
    var pdfUrl:String = ""
    private var isError = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setupViews()
    }
    
    private func setupViews(){
        let webConfiguration = WKWebViewConfiguration()
        self.webView = WKWebView(frame: self.webViewContainer.bounds, configuration: webConfiguration)
        self.webView.isOpaque = false
        
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        self.webViewContainer.addSubview(self.webView)
        
        self.setupWebView()
    }
    
    private func setupWebView(){
        
        let myURL = URL(string: self.pdfUrl)
        let myRequest = URLRequest(url: myURL!)
        self.webView.load(myRequest)
    }
    
    private func setupTableView(){
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.tableView.isHidden = true
        self.loadingStackView.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("FAILED")
        self.isError = true
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Catalog Detail"
    }
    
    
}

extension CatalogDetailViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isError{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell") as! ErrorTableViewCell
        cell.delegate = self
        return cell
    }
}

extension CatalogDetailViewController: ErrorDelegate{
    
    func retryAction() {
        self.tableView.isHidden = false
        self.tableView.reloadData()
        self.isError = false
        
        self.setupWebView()
    }
    
}
