//
//  SecondRegisterViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SVProgressHUD

class SecondRegisterViewController: KanaViewController {
    
    @IBOutlet var honoroficTextField: UITextField!
    @IBOutlet var firstNameTextField: KanaTextField!
    @IBOutlet var lastNameTextField: KanaTextField!
    @IBOutlet var dateOfBirthTextField: KanaTextField!
    
    @IBOutlet var firstNameWarningLabel: UILabel!
    
    @IBOutlet var registerButton: KanaButton!
    
    private var disposeBag = DisposeBag()
    var honoroficList = ["Bpk.", "Dr.", "Ibu", "Miss", "Mr.", "Mrs.","Ms."]
    var honoroficPickerView = UIPickerView()
    
    private var isFirstNameValidated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        self.observeViewModel()
    }
    
    private func setupDefaultValue(){
        
        if let firstName = UserInput.firstName{
            firstNameTextField.text = firstName
            checkFirstName { (isValidated) in
                if isValidated{
                    firstNameWarningLabel.isHidden = true
                }else{
                    firstNameWarningLabel.isHidden = false
                }
            }
        }
        
        if let lastName = UserInput.lastName{
            lastNameTextField.text = lastName
        }
        
        if let dateOfBirth = UserInput.dateOfBirth{
            dateOfBirthTextField.text = dateOfBirth
            checkFirstName { (isValidated) in
                if isValidated{
                    toggleRegisterButton(enabled: true)
                }
            }
        }
    }
    
    private func setupViews(){
        
        self.title = "REGISTER"
        
        hideKeyboardWhenTappedAround()
        
        firstNameTextField.kanaTextFieldDegate = self
        lastNameTextField.kanaTextFieldDegate = self
        
        let toolbar = PickerViewToolbar(delegate: self, screenWidth: view.frame.width)
        
        // Setup HonoroficPickerField
        honoroficPickerView.delegate = self
        honoroficTextField.inputView = honoroficPickerView
        honoroficTextField.inputAccessoryView = toolbar
        honoroficTextField.tintColor = .clear
        // END
        
        let bodDatePicker = KanaDatePicker(delegate: self)
        if #available(iOS 13.4, *) {
            bodDatePicker.preferredDatePickerStyle = .wheels
        }
        dateOfBirthTextField.inputView = bodDatePicker
        dateOfBirthTextField.inputAccessoryView = toolbar
        dateOfBirthTextField.tintColor = .clear
        
        setupDefaultValue()
    }
    
    private func observeViewModel(){
        AuthViewModel.shared.isRegisterSuccess.asObservable().bind { (isSuccess) in
            
            if isSuccess{
                SVProgressHUD.showSuccess(withStatus: "Register Success.")
                SVProgressHUD.dismiss(withDelay: Delay.SHORT) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            
        }.disposed(by: self.disposeBag)
        
        AuthViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            guard let errorMsg = errorMsg else {return}
            SVProgressHUD.showError(withStatus: errorMsg)
            SVProgressHUD.dismiss(withDelay: Delay.SHORT)
        }.disposed(by: self.disposeBag)
    }
    
    func checkFirstName(completion:(_ isValidated:Bool)->Void){
        if let firstName = UserInput.firstName, firstName.count >= 2, firstName.count <= 35{
            completion(true)
        }else{
            firstNameWarningLabel.isHidden = false
            completion(false)
        }
    }
    
    @IBAction func registerAction(_ sender: Any) {
        SVProgressHUD.show()
        AuthViewModel.shared.register(fullName: "\(firstNameTextField.text!) \(lastNameTextField.text!)", honorificName: honoroficTextField.text!, password: UserInput.password!, email: UserInput.email!, phone: UserInput.phone!, birthdate: self.dateOfBirthTextField.text!)
    }
}

extension SecondRegisterViewController:UITextFieldDelegate, KanaTextFieldDelegate{
    
    func didValueChanged(sender: UITextField) {
        print(sender)
        switch sender {
        case firstNameTextField:
            if firstNameTextField.text!.count >= 2, firstNameTextField.text!.count <= 35{
                isFirstNameValidated = true
                firstNameWarningLabel.isHidden = true
            }else{
                isFirstNameValidated = false
                firstNameWarningLabel.isHidden = false
            }
        default:
            print("ABC")
            break
        }
        inputValidator()
    }
    
    private func inputValidator(){
        if isFirstNameValidated, UserInput.dateOfBirth != nil{
            toggleRegisterButton(enabled: true)
        }else{
            toggleRegisterButton(enabled: false)
        }
    }
    
    private func toggleRegisterButton(enabled:Bool){
        if enabled{
            registerButton.isEnabled = true
            registerButton.backgroundColor = .black
        }else{
            registerButton.isEnabled = false
            registerButton.backgroundColor = Color.PRIMARY_COLOR
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            dateOfBirthTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case firstNameTextField:
            UserInput.firstName = firstNameTextField.text
        case lastNameTextField:
            UserInput.lastName = lastNameTextField.text
        default:
            break
        }
    }
}

extension SecondRegisterViewController:UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return honoroficList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return honoroficList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        honoroficTextField.text = honoroficList[row]
    }
}

extension SecondRegisterViewController:ToolbarDelegate{
    func doneAction() {
        self.dismissKeyboard()
    }
}

extension SecondRegisterViewController:DatePickerDelegate{
    func didSelectDate(dateString: String) {
        self.dateOfBirthTextField.text = dateString
        UserInput.dateOfBirth = dateString
        inputValidator()
    }
}
