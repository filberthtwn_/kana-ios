//
//  PromoViewModel.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct PromoViewModel{
    static let shared = PromoViewModel()
    let promoHighlight:BehaviorRelay<PromoHighlight?> = BehaviorRelay(value: nil)
    let promoData:PublishSubject<PromoData?> = PublishSubject()
    let promoDetail:PublishSubject<PromoData?> = PublishSubject()
    let isSuccess:PublishSubject<Bool> = PublishSubject()
    let errorMsg:PublishSubject<String?> = PublishSubject()

    func getPromo(lastId:String, limit:Int){
        let url = "/getPromo"
        let params:[String:Any] = [
            "last_id":lastId,
            "limit":limit,
            "highlight":""
        ]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(PromoResponse.self, from: data)
                        if let promoData = response.data{
                            if promoData.error != ""{
                                self.errorMsg.onNext(promoData.error)
                            }else{
                                self.promoData.onNext(promoData)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                 self.errorMsg.onNext(error)
            }
        }
    }
    
    func getPromoDetail(promoId:String){
        let url = "/getPromoDetail"
        let params:[String:Any] = [
            "IdPromoDetail": promoId
        ]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(PromoResponse.self, from: data)
                        if let promoData = response.data{
                            if promoData.error != ""{
                                self.errorMsg.onNext(promoData.error)
                            }else{
                                self.promoDetail.onNext(promoData)
                            }
                        }
                    }
                }catch (let error){
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
}
