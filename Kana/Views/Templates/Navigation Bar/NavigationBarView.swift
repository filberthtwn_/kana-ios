//
//  NavigationBarView.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

protocol NavigationDelegate {
    func backAction()
}
class NavigationBarView: UIView {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var view: UIView!
    var delegate:NavigationDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
         Bundle.main.loadNibNamed("NavigationBarView", owner: self, options: nil)
        addSubview(view)
    }
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
}
