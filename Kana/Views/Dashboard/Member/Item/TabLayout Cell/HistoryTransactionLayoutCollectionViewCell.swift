//
//  HistoryTransactionLayoutCollectionViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

protocol TransactionHistoryDelegate {
    func didTransactionSelected(salesId:String)
    func seeAllHistoryAction()
}

class HistoryTransactionLayoutCollectionViewCell: UICollectionViewCell {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var tableViewHeightConstraint: NSLayoutConstraint!
    
    var transactions:[Transaction] = []
    var delegate:TransactionHistoryDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTableView()
    }
    
    private func setupTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
        
        self.tableView.register(UINib(nibName: "HistoryTransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "HistoryTransactionCell")
    }
    
    override func prepareForReuse() {
        tableViewHeightConstraint.constant = CGFloat(138 * transactions.count)
    }
    
    
    @IBAction func seeAllHistoryAction(_ sender: Any) {
        self.delegate?.seeAllHistoryAction()
    }
}

extension HistoryTransactionLayoutCollectionViewCell:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didTransactionSelected(salesId: self.transactions[indexPath.row].salesId)
    }
}

extension HistoryTransactionLayoutCollectionViewCell:UITableViewDataSource{
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTransactionCell") as! HistoryTransactionTableViewCell
        let transaction = transactions[indexPath.item]
        cell.salesIdLabel.text = transaction.salesId
        cell.transferDateLabel.text = transaction.transferDate
        cell.receiptIdLabel.text = transaction.receiptId
        cell.commentLabel.text = transaction.comment
        cell.amountLabel.text = transaction.amount

        return cell
    }
}
