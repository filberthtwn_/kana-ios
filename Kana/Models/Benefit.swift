//
//  Benefit.swift
//  Kana
//
//  Created by Filbert Hartawan on 09/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

struct Benefit:Codable {
    var orderNo:String = ""
    var description:String = ""
    
    enum CodingKeys:String, CodingKey{
        case orderNo = "ORDERNO"
        case description = "DESCRIPTION"
    }
}

struct BenefitItem:Codable {
    var benefits:[Benefit] = []
    
    enum CodingKeys:String, CodingKey {
        case benefits = "BENEFIT"
    }
}

struct BenefitData:Codable {
    var benefitItem:BenefitItem?
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case benefitItem = "BENEFITS"
        case error = "ERROR"
    }
}

struct BenefitResponse:Codable{
    var data: BenefitData?
    
    enum CodingKeys:String, CodingKey {
        case data = "DATA"
    }
}
