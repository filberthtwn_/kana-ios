//
//  MembershipTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 02/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

protocol MembershipCardProtocol {
    func signInButtonAction()
}

class MembershipTableViewCell: UITableViewCell {

    @IBOutlet var cardIV: UIImageView!
    
    @IBOutlet var cardDataView: UIStackView!
    @IBOutlet var cardView: UIView!
    @IBOutlet var signInButton: UIButton!
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var tierLabel: UILabel!
    @IBOutlet var cardIdLabel: UILabel!
    @IBOutlet var expireDateLabel: UILabel!
    
    var delegate:MembershipCardProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.cardView.layer.cornerRadius = 10
        self.cardView.layer.masksToBounds = true
        self.signInButton.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func signInAction(_ sender: Any) {
        self.delegate?.signInButtonAction()
    }
    
}
