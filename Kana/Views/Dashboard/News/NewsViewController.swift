//
//  NewsViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Differentiator
import AlamofireImage
import SVProgressHUD

enum NewsMultipleSectionModel{
    case ErrorSection(header:String, items:[NewsSectionItem])
    case ShimmerSection(header:String, items:[NewsSectionItem])
    case NewsSection(header:String, items:[NewsSectionItem])
}

enum NewsSectionItem{
    case ErrorSectionItem
    case ShimmerSectionItem
    case NewsSectionItem(news:News)
}

extension NewsMultipleSectionModel: SectionModelType {
    typealias Item = NewsSectionItem
    
    var items: [NewsSectionItem] {
        switch  self {
        case .ErrorSection(header: _, items: let items):
            return items.map { $0 }
        case .ShimmerSection(header: _, items: let items):
            return items.map { $0 }
        case .NewsSection(header: _, items: let items):
            return items.map { $0 }
        }
    }
    
    init(original: NewsMultipleSectionModel, items: [Item]) {
        switch original {
        case let .ErrorSection(header: header, items: _):
            self = .ErrorSection(header: header, items: items)
        case let .ShimmerSection(header: header, items: _):
            self = .ShimmerSection(header: header, items: items)
        case let .NewsSection(header, _):
            self = .NewsSection(header: header, items: items)
        }
    }
}

class NewsViewController: UIViewController {

    
    @IBOutlet var tableView: UITableView!
    
    private var refreshControl = UIRefreshControl()
    private var isDataLoaded = false
    private var isLoading = false
    private var isMaximum = false
    
    private var newsData:NewsData?
    private var newsList:[News] = []
    private var lastId:BehaviorRelay<String> = BehaviorRelay(value: "")
    private var limit:BehaviorRelay<Int> = BehaviorRelay(value: 50)
    
    private var spinner = UIActivityIndicatorView(style: .gray)
    private let disposeBag = DisposeBag()
    private var sections:BehaviorRelay<[NewsMultipleSectionModel]> = BehaviorRelay.init(value: [
           .ShimmerSection(header: "shimmer", items: [
               .ShimmerSectionItem,
               .ShimmerSectionItem,
               .ShimmerSectionItem,
               .ShimmerSectionItem,
               .ShimmerSectionItem
           ])
       ])
    private var dataSource:RxTableViewSectionedReloadDataSource<NewsMultipleSectionModel>?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.observeViewModel()
    }
    
    private func setupViews(){
        self.setupTableView()
    }
    
    private func setupTableView(){
        
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
        self.tableView.register(UINib(nibName: "ShimmerTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        self.tableView.register(UINib(nibName: "DashboardTableViewCell", bundle: nil), forCellReuseIdentifier: "DashboardCell")
        self.tableView.register(UINib(nibName: "DashboardTableViewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "DashboardHeader")
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.dataSource = RxTableViewSectionedReloadDataSource<NewsMultipleSectionModel>(
            configureCell: { dataSource, tableView, indexPath, item in
                
                switch (dataSource[indexPath]){
                case .ShimmerSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell") as! ShimmerTableViewCell
                    return cell
                case let .NewsSectionItem(news):
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCell") as! DashboardTableViewCell
                    cell.titleLabel.text = "\(news.title)"
                    cell.descriptionLabel.text = "\(news.subtitle)"
                    
                    if let imageUrl =  URL(string: news.imageUrl){
                        cell.cardImageView.af.setImage(withURL: imageUrl, placeholderImage: UIImage(named: "defaultImage"))
                    }
                    
                    let news = self.sections.value[0].items
                    if indexPath.item == (news.count-1){
                        cell.separatorLine.isHidden = true
                    }
                    
                    return cell
                case .ErrorSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell") as! ErrorTableViewCell
                    cell.delegate = self
                    return cell
                }
        })
        
        self.tableView
        .rx
        .setDelegate(self)
        .disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            let newsDetailVC = NewsDetailViewController()
            if let newsData = self.newsData{
                newsDetailVC.newsId = newsData.newsList[indexPath.item].id
            }
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
        }).disposed(by: disposeBag)
        
        self.sections
            .bind(to: tableView.rx.items(dataSource: self.dataSource!))
            .disposed(by: disposeBag)
        
        // MARK: Observe Last Id for Infinite Scrolling
        self.lastId.asObservable()
            .bind { (start)  in
            NewsViewModel.shared.getNews(lastId: self.lastId.value, limit: self.limit.value)
        }.disposed(by: disposeBag)
        // END
    }
    
    private func observeViewModel(){
        
        NewsViewModel.shared.newsData.asObservable().bind { (newsData) in
            self.refreshControl.endRefreshing()

            if let newsData = newsData{
                
                var sections:[NewsMultipleSectionModel] = []
                
                self.newsData = newsData
                self.isDataLoaded = true

                if self.sections.value.count == 0{
                    self.newsList = newsData.newsList
                }else{
                    
                    if newsData.newsList.count > 0{
                        self.newsList.append(contentsOf: newsData.newsList)
                        self.isLoading = false
                    }else{
                        self.toggleSpinner(show: false)
                        self.isMaximum = true
                    }
                }
                
                var newsSectionItems:[NewsSectionItem] = []
                for news in self.newsList{
                    let newsItem:NewsSectionItem = .NewsSectionItem(news: news)
                    newsSectionItems.append(newsItem)
                }
                
                let section:NewsMultipleSectionModel = .NewsSection(header: "", items: newsSectionItems)
                sections.append(section)
                self.sections.accept(sections)

                
            }
        }.disposed(by: disposeBag)
        
        NewsViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg {
                var sections:[NewsMultipleSectionModel] = []
                let section:NewsMultipleSectionModel = .ErrorSection(header: "", items: [
                    .ErrorSectionItem
                ])
                sections.append(section)
                self.sections.accept(sections)
                
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = self.tableView.contentSize.height
//
//        if let newsData = self.newsData, let lastNews = newsData.newsList.last{
//            if contentHeight > 0, offsetY >= (contentHeight - self.tableView.frame.size.height){
//                if !self.isLoading && !isMaximum{
//                    self.toggleSpinner(show: true)
//                    self.lastId.accept(lastNews.id)
//                    self.isLoading = true
//                }
//            }
//        }
        
    }
    
    // MARK: Toggle Spinner for Infinity Scrolling
    func toggleSpinner(show:Bool){
        if show{
            //MARK: Show TableView Spinner Footer
            self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(50))
            self.tableView.tableFooterView = spinner
            self.spinner.startAnimating()
            // END
        }else{
            //MARK: Hide TableView Spinner Footer
            self.tableView.tableFooterView = nil
            // END
        }
    }
    // END
    
    
    @objc func refresh(_ sender: AnyObject) {
        self.lastId.accept("")
    }
    
}

extension NewsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.isDataLoaded{
            return self.tableView.frame.height + 16
        }else{
            return 16
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if self.isDataLoaded{
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "DashboardHeader") as! DashboardTableViewHeaderView
            
            if let newsData = self.newsData, let newsHighlight = newsData.newsHighlight{
                header.titleLabel.text = newsHighlight.title
                if let imageUrl = URL(string: newsHighlight.imageUrl.replacingOccurrences(of: " ", with: "%20")){
                    header.backgroundIV.af.setImage(withURL: imageUrl)
                }
                header.subtitleLabel.text = newsHighlight.subtitle.htmlToString.uppercased()
            }
            return header
        }else{
            return UIView()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
}

extension NewsViewController: ErrorDelegate{
    
    func retryAction() {
        var sections:[NewsMultipleSectionModel] = []
        let shimmer:NewsMultipleSectionModel = .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem
        ])
        sections.append(shimmer)
        self.sections.accept(sections)
        
        self.lastId.accept("")
    }

}
