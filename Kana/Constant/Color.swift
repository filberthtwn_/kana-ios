//
//  Color.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import UIKit

struct Color {
    static let PRIMARY_COLOR = UIColor.init(red: 131/255.0, green: 131/255.0, blue: 131/255.0, alpha: 1)
    static let SECONDARY_COLOR = UIColor.init(red: 246/255.0, green: 246/255.0, blue: 246/255.0, alpha: 1)
    static let BORDER_COLOR = UIColor.init(red: 221/255.0, green: 221/255.0, blue: 221/255.0, alpha: 1).cgColor
    static let BACKGROUND_PRIMARY = UIColor.init(red: 246/255.0, green: 246/255.0, blue: 246/255.0, alpha: 1)
}
