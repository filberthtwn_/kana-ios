//
//  BenefitTableViewFooterView.swift
//  Kana
//
//  Created by Filbert Hartawan on 02/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

protocol BenefitDelegate {
    func faqAction()
    func myProfileAction()
    func changePasswordAction()
    func notificationAction()
    func loginAction()
}

class BenefitTableViewFooterView: UITableViewHeaderFooterView {
    
    var delegate:BenefitDelegate?
    
    @IBOutlet var appVersionLabel: UILabel!
    
    @IBOutlet var faqButton: UIButton!
    @IBOutlet var myProfileButton: UIButton!
    @IBOutlet var changePasswordButton: UIButton!
    @IBOutlet var notificationButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    
    @IBAction func faqAction(_ sender: Any) {
        self.delegate?.faqAction()
    }
    
    @IBAction func myProfileAction(_ sender: Any) { self.delegate?.myProfileAction()
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        self.delegate?.changePasswordAction()
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        self.delegate?.notificationAction()
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.delegate?.loginAction()
    }
}
