//
//  PickerViewToolbar.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

protocol ToolbarDelegate {
    func doneAction()
}

class PickerViewToolbar: UIToolbar {
    var toolbarDelegate:ToolbarDelegate?
    var screenWidth:CGFloat = 0
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupViews()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    init(delegate:ToolbarDelegate, screenWidth:CGFloat) {
        self.init()
        self.screenWidth = screenWidth
        self.toolbarDelegate = delegate
        setupViews()
    }
    
    private func setupViews(){
        self.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 44)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
        self.barStyle = .default
        self.isTranslucent = false
        self.items = [spaceButton, btnDone]
    }
    
    @objc private func doneAction(){
        toolbarDelegate?.doneAction()
    }
}
