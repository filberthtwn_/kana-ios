//
//  KanaDatePicker.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import UIKit

protocol DatePickerDelegate {
    func didSelectDate(dateString:String)
}

class KanaDatePicker: UIDatePicker {
    
    var delegate:DatePickerDelegate?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupViews()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    init(delegate:DatePickerDelegate) {
        self.init()
        self.delegate = delegate
        setupViews()
    }
    
    private func setupViews(){
        self.datePickerMode = .date
        self.addTarget(self, action: #selector(didSelectDate), for: .valueChanged)
        self.maximumDate = Date()
    }
    
    @objc private func didSelectDate(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: sender.date)
        delegate?.didSelectDate(dateString: dateString)
    }
}
