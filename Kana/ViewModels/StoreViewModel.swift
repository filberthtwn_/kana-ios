//
//  StoreViewModel.swift
//  Kana
//
//  Created by Filbert Hartawan on 09/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct StoreViewModel{
    static let shared = StoreViewModel()
    let storeHighlight:BehaviorRelay<StoreHighlight?> = BehaviorRelay(value: nil)
    let storeListData:PublishSubject<StoreData?> = PublishSubject()
    let storeData:PublishSubject<StoreData?> = PublishSubject()
    let isSuccess:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let errorMsg:BehaviorRelay<String?> = BehaviorRelay(value: nil)

    func getStore(city:String){
        let url = "/getStore"
        let params:[String:Any] = [
            "city":""
        ]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(StoreResponse.self, from: data)
                        if let storeData = response.data{
                            if storeData.error != ""{
                                self.errorMsg.accept(storeData.error)
                            }else{
                                self.storeListData.onNext(storeData)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.accept(error.localizedDescription)
                }
            }else{
                self.errorMsg.accept(error)
            }
        }
    }
    
    func getStoreDetail(storeId:String){
        let url = "/getStoreDetail"
        let params:[String:Any] = [
            "storeId":storeId,
            "last_Id":"0",
            "limit":"1"
        ]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(StoreResponse.self, from: data)
                        if let storeData = response.data{
                            if storeData.error != ""{
                                self.errorMsg.accept(storeData.error)
                            }else{
                                print(storeData)
                                self.storeData.onNext(storeData)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    print("ABCC")
                    self.errorMsg.accept(error.localizedDescription)
                }
            }else{
                self.errorMsg.accept(error)
            }
        }
    }
}
