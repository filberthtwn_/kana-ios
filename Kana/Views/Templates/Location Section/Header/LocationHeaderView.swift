//
//  PromoHeaderView.swift
//  Kana
//
//  Created by Filbert Hartawan on 14/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class LocationHeaderView: UITableViewHeaderFooterView {

    @IBOutlet var titleLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
