//
//  TransactionDetailListTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 22/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class TransactionDetailListShimmerTableViewCell: UITableViewCell {
    @IBOutlet var shimmer: KanaFBShimmerView!
    @IBOutlet var shimmerContent: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.shimmer.contentView = shimmerContent
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
