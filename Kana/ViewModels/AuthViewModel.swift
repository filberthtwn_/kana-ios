//
//  AuthViewModel.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

struct AuthViewModel{
    static let shared = AuthViewModel()
    let email:BehaviorRelay<String> = BehaviorRelay(value: "")
    let password:BehaviorRelay<String> = BehaviorRelay(value: "")
    
    let isLoginSuccess:PublishSubject<Bool> = PublishSubject()
    let isRegisterSuccess:PublishSubject<Bool> = PublishSubject()
    let isLogoutSuccess:PublishSubject<Bool> = PublishSubject()
    let isForgotPasswordSuccess:PublishSubject<Bool> = PublishSubject()
    
    let errorMsg:PublishSubject<String?> =  PublishSubject()

    func login(){
        let url = "/MbrAppLogin"
        let params:[String:Any] = [
            "strEmail": email.value,
            "strPassword": password.value,
            "tokenFCM": FCMToken.token
        ]
                
        PublicService.shared.post(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let json = try JSON(data: data)
                        let error = json["DATA"]["ERROR"].stringValue
                        if error != "" {
                            self.errorMsg.onNext(error)
                        }else{
                            do {
                                let response = try JSONDecoder().decode(UserResponse.self, from: data)
                                if let userData = response.data, let user = userData.customer{
                                    if let encodedUser = try? JSONEncoder().encode(user){
                                        UserDefaultHelper.shared.setupUser(data: encodedUser)
                                    }
                                    self.isLoginSuccess.onNext(true)
                                }
                            } catch (let error) {
                                self.errorMsg.onNext(error.localizedDescription)
                            }
                        }
                    }
                }catch (let error){
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
    
    func register(fullName:String, honorificName:String, password:String, email:String, phone:String, birthdate:String){
        let url = "/CustomerRegistration"
        let params:[String:Any] = [
            "userName":fullName,
            "userPwd":password,
            "userMail":email,
            "userPhone":phone,
            "DOB":birthdate,
            "userNamePrefix":honorificName
        ]
        
        PublicService.shared.post(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let json = try JSON(data: data)
                        let error = json["USERREGISTRATION"]["ERROR"].stringValue
                        if error != "" {
                            self.errorMsg.onNext(error)
                        }else{
                            self.isRegisterSuccess.onNext(true)
                        }
                    }
                }catch (let error){
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
    
    func logout(token:String){
        let url = "/MbrAppLogout"
        let params:[String:Any] = [
            "token":token
        ]
        
        PublicService.shared.post(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let json = try JSON(data: data)
                        let error = json["DATA"]["ERROR"].stringValue
                        if error != "" {
                            self.errorMsg.onNext(error)
                        }else{
                            self.isLogoutSuccess.onNext(true)
                        }
                    }
                }catch (let error){
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
    
    func forgotPassword(email:String){
        let url = "/MbrAppResetPwd"
        let params:[String:Any] = [
            "email":email
        ]
        
        PublicService.shared.post(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let json = try JSON(data: data)
                        let error = json["DATA"]["ERROR"].stringValue
                        if error != "" {
                            self.errorMsg.onNext(error)
                        }else{
                            self.isForgotPasswordSuccess.onNext(true)
                        }
                    }
                }catch (let error){
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
    
    func updatePassword(oldPassword:String, newPassword:String, confirmNewPassword:String, token:String){
        let url = "/CustAppsChangePassword"
        let params:[String:Any] = [
            "OldPassword" : oldPassword,
            "NewPassword" :newPassword,
            "ConfirmNewPassword" : confirmNewPassword,
            "Token" : token
        ]
        PublicService.shared.post(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let json = try JSON(data: data)
                        let error = json["DATA"]["ERROR"].stringValue
                        if error != "" {
                            self.errorMsg.onNext(error)
                        }else{
                            self.isLoginSuccess.onNext(true)
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                 self.errorMsg.onNext(error)
            }
        }
    }
}
