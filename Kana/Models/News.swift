//
//  News.swift
//  Kana
//
//  Created by Filbert Hartawan on 09/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

struct News:Decodable {
    var id:String = ""
    var content:String = ""
    var title:String = ""
    var subtitle:String = ""
    var imageUrl:String = ""
    var imageUrls:NewsImage = NewsImage()
    var stores:[Store] = []
    
    enum CodingKeys:String, CodingKey{
        case id = "ID"
        case content = "CONTEN"
        case title = "TITLE"
        case subtitle = "SUBTITLE"
        case imageUrl = "IMAGE"
        case imageUrls = "Image"
        case stores = "STORES"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let id = try container.decodeIfPresent(String.self, forKey: .id){
            self.id = id
        }
        if let content = try container.decodeIfPresent(String.self, forKey: .content){
            self.content = content
        }
        if let title = try container.decodeIfPresent(String.self, forKey: .title){
            self.title = title
        }
        if let subtitle = try container.decodeIfPresent(String.self, forKey: .subtitle){
            self.subtitle = subtitle
        }
        if let imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl){
            self.imageUrl = imageUrl
        }
        if let imageUrls = try container.decodeIfPresent(NewsImage.self, forKey: .imageUrls){
            self.imageUrls = imageUrls
        }
        do {
            if let stores = try container.decodeIfPresent([Store].self, forKey: .stores){
                self.stores = stores
            }
        } catch (let error) {
            print(error)
            if let store = try container.decodeIfPresent(Store.self, forKey: .stores){
                self.stores.append(store)
            }
        }
    }
}

struct NewsImage:Decodable{
    var imageUrl:String = ""
    
    enum CodingKeys:String, CodingKey {
        case imageUrl = "IMAGE"
    }
}

struct NewsHighlight:Codable {
    var title:String?
    var subtitle:String = ""
    var imageUrl: String = ""
    
    enum CodingKeys:String, CodingKey {
        case title = "TITLE"
        case subtitle = "SUBTITLE"
        case imageUrl = "IMAGE"
    }
}

struct NewsData:Decodable {
    var newsHighlight:NewsHighlight?
    var newsList:[News] = []
    var news:News?
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case newsHighlight = "HIGHLIGHTNEWS"
        case news = "NEWS"
        case error = "ERROR"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            if let newsHighlight = try container.decodeIfPresent(NewsHighlight?.self, forKey: .newsHighlight){
                self.newsHighlight = newsHighlight
            }
        } catch (let error) {
            print(error)
        }
        
        do {
            if let newsList = try container.decodeIfPresent([News].self, forKey: .news){
                self.newsList = newsList
            }
        } catch (let error){
            print(error)
            if let news = try container.decodeIfPresent(News.self, forKey: .news){
                self.newsList.append(news)
            }
        }
        
        do {
            if let news = try container.decodeIfPresent(News.self, forKey: .news){
               self.news = news
            }
        } catch(let error) {
            print(error)
        }
        
        
        self.error = try container.decode(String.self, forKey: .error)
    }
}

struct NewsResponse:Decodable{
    var data: NewsData?
    
    enum CodingKeys:String, CodingKey {
        case data = "DATA"
    }
}
