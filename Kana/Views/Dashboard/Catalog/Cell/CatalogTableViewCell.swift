//
//  CatalogTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class CatalogTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var cardImageView: UIImageView!
    
    @IBOutlet var separatorLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
