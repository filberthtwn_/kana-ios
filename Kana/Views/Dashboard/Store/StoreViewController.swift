//
//  StoreViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Differentiator
import AlamofireImage
import SVProgressHUD

enum StoreMultipleSectionModel{
    case ErrorSection(header:String, items:[StoreSectionItem])
    case ShimmerSection(header:String, items:[StoreSectionItem])
    case ContactSection(header:String, items:[StoreSectionItem])
    case StoreSection(header:String, items:[StoreSectionItem])
}

enum StoreSectionItem{
    case ErrorSectionItem
    case ShimmerSectionItem
    case ContactSectionItem(instagram:String, facebook:String, youtube:String, website:String, email:String)
    case StoreSectionItem(stores:Store)
}

extension StoreMultipleSectionModel: SectionModelType {
    typealias Item = StoreSectionItem
    
    var items: [StoreSectionItem] {
        switch  self {
        case .ErrorSection(header: _, items: let items):
            return items.map { $0 }
        case .ShimmerSection(header: _, items: let items):
            return items.map { $0 }
        case .ContactSection(header: _, items: let items):
            return items.map { $0 }
        case .StoreSection(header: _, items: let items):
            return items.map { $0 }
        }
    }
    
    init(original: StoreMultipleSectionModel, items: [Item]) {
        switch original {
        case let .ErrorSection(header: header, items: _):
            self = .ErrorSection(header: header, items: items)
        case let .ShimmerSection(header: header, items: _):
            self = .ShimmerSection(header: header, items: items)
        case let .ContactSection(header: header, items: _):
            self = .ContactSection(header: header, items: items)
        case let .StoreSection(header, _):
            self = .StoreSection(header: header, items: items)
        }
    }
}

extension StoreMultipleSectionModel {
    var header: String {
        switch self {
        case .ErrorSection(header: let title, items: _):
            return title
        case .ShimmerSection(header: let title, items: _):
            return title
        case .ContactSection(header: let title, items: _):
            return title
        case .StoreSection(header: let title, items: _):
            return title
        }
    }
}

class StoreViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    private var refreshControl = UIRefreshControl()
    private var isDataLoaded = false
    
    private var storeData:StoreData? = nil
    private var spinner = UIActivityIndicatorView(style: .gray)
    private var disposeBag = DisposeBag()
    private var sections:BehaviorRelay<[StoreMultipleSectionModel]> = BehaviorRelay.init(value: [
        .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem
        ])
    ])
    private var dataSource:RxTableViewSectionedReloadDataSource<StoreMultipleSectionModel>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
        self.observeViewModel()
        self.setupTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func setupTableView(){
        self.tableView
        .rx
        .setDelegate(self)
        .disposed(by: disposeBag)
        
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
        self.tableView.register(UINib(nibName: "ShimmerTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        self.tableView.register(UINib(nibName: "DashboardTableViewCell", bundle: nil), forCellReuseIdentifier: "DashboardCell")
        self.tableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactCell")
        self.tableView.register(UINib(nibName: "DashboardTableViewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "DashboardHeader")
        self.tableView.register(UINib(nibName: "StoreTableViewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "StoreHeader")
        self.tableView.register(UINib(nibName: "StoreTableViewFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "StoreFooter")
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.dataSource = RxTableViewSectionedReloadDataSource<StoreMultipleSectionModel>(
            configureCell: { dataSource, tableView, indexPath, item in
                switch dataSource[indexPath] {
                    
                case .ShimmerSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell") as! ShimmerTableViewCell
                    return cell
                    
                case let .ContactSectionItem(instagram, facebook, youtube, website, email):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactTableViewCell
                    
                    cell.instagramLabel.text = instagram
                    cell.facebookLabel.text = facebook
                    cell.youtubeLabel.text = youtube
                    cell.websiteLabel.text = website
                    cell.emailLabel.text = email
                                        
                    return cell
                case let .StoreSectionItem(store):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCell") as! DashboardTableViewCell
                    cell.titleLabel.text = store.name
                    cell.descriptionLabel.text = store.address
                                                            
                    if let imageUrl =  URL(string: store.imageUrl){
                        cell.cardImageView.af.setImage(withURL: imageUrl, placeholderImage: UIImage(named: "defaultImage"))
                    }
                    
                    if let storeData = self.storeData{
                        if indexPath.item == (storeData.stores.count-1){
                            cell.separatorLine.isHidden = true
                        }
                    }

                    return cell
                case .ErrorSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell") as! ErrorTableViewCell
                    cell.delegate = self
                    return cell
                }
            }
        )
        
        self.tableView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            if indexPath.section == 1{
                if let storeData = self.storeData{
                    let storeDetailVC = StoreDetailViewController()
                    storeDetailVC.storeId = (storeData.stores[indexPath.item].id)
                    self.navigationController?.pushViewController(storeDetailVC, animated: true)
                }
            }
        }).disposed(by: disposeBag)
        
        self.sections.asObservable()
            .bind(to: tableView.rx.items(dataSource: self.dataSource!))
            .disposed(by: disposeBag)
    }
    
    private func setupData(){
        StoreViewModel.shared.getStore(city: "")
    }
    
    private func observeViewModel(){
        StoreViewModel.shared.storeListData.bind { (storeData) in
            
            self.refreshControl.endRefreshing()

            if let storeData = storeData{
                self.storeData = storeData
                
                var sections:[StoreMultipleSectionModel] = []
                
                if let storeHighlight = storeData.storeHighlight{
                    let contactSection:StoreMultipleSectionModel = .ContactSection(header: "", items: [
                        .ContactSectionItem(instagram: storeHighlight.instagram, facebook: storeHighlight.facebook, youtube: storeHighlight.youtube, website: storeHighlight.website, email: storeHighlight.email)
                    ])
                    sections.append(contactSection)
                }
                
                var storeSectionItems:[StoreSectionItem] = []
                for store in storeData.stores {
                    let storeSectionItem:StoreSectionItem = .StoreSectionItem(stores: store)
                    storeSectionItems.append(storeSectionItem)
                }
                let storeSection:StoreMultipleSectionModel = .StoreSection(header: "", items: storeSectionItems)
                sections.append(storeSection)
                
                self.sections.accept(sections)
                self.storeData = storeData
                self.isDataLoaded = true
            }
        }.disposed(by: disposeBag)
        
        StoreViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg{
                var sections:[StoreMultipleSectionModel] = []
                let section:StoreMultipleSectionModel = .ErrorSection(header: "", items: [
                    .ErrorSectionItem
                ])
                sections.append(section)
                self.sections.accept(sections)
                
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.setupData()
    }
}

extension StoreViewController: UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section{
        case 0:
            return 1
        default:
            return 2
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if self.isDataLoaded{
            switch section{
            case 0:
                return self.view.frame.height + 16
            default:
                return UITableView.automaticDimension
            }
        }else{
            return 16
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if self.isDataLoaded{
            switch section {
            case 0:
                let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "DashboardHeader") as! DashboardTableViewHeaderView
                
                
                if let storeData = self.storeData, let storeHighlight = storeData.storeHighlight{
                    header.titleLabel.text = storeHighlight.title
                    if let imageUrl = URL(string: storeHighlight.imageUrl){
                        header.backgroundIV.af.setImage(withURL: imageUrl)
                    }
                    header.subtitleLabel.text = storeHighlight.subtitle.htmlToString.uppercased()
                }
                
                return header
            default:
                let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "StoreHeader") as! StoreTableViewHeaderView
                return header
            }
        }else{
            return UIView()
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if self.isDataLoaded{
            if section == 1{
                let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "StoreFooter") as! StoreTableViewFooterView
                if let storeData = self.storeData, let storeHighlight = storeData.storeHighlight{
                    footer.dekorumaLink = storeHighlight.dekorumaLink
                    footer.tokopediaLink = storeHighlight.tokopediaLink
                    footer.websiteLink = storeHighlight.website
                }
                return footer
            }
        }
        return nil
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if self.isDataLoaded{
            if section > 0{
                return UITableView.automaticDimension
            }
        }
        return .leastNonzeroMagnitude
        
    }
}

extension StoreViewController: ErrorDelegate{
    
    func retryAction() {
        var sections:[StoreMultipleSectionModel] = []
        let shimmer:StoreMultipleSectionModel = .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem
        ])
        sections.append(shimmer)
        self.sections.accept(sections)
        
        self.setupData()
    }

}
