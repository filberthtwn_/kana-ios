//
//  String+Ext.swift
//  Kana
//
//  Created by Filbert Hartawan on 09/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var htmlToAttributedString: NSAttributedString? {
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue';font-size: 14px;color:#5F5F5F\">%@</span>", self)
        
        do {
            return try NSAttributedString(data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
                                          options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
