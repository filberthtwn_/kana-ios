//
//  PromoDescriptionTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 14/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var periodLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    @IBOutlet var promoImageCV: UICollectionView!
    @IBOutlet var promoImagePageControl: UIPageControl!
    
    var promoImages:[PromoImage] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        self.promoImagePageControl.numberOfPages = self.promoImages.count
    }
    
    private func setupViews(){
        self.setupCollectionView()
    }
    
    private func setupCollectionView(){
        self.promoImageCV.delegate = self
        self.promoImageCV.dataSource = self
        self.promoImageCV.register(UINib(nibName: "DescriptionImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let selectedPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
        self.updatePageControl(selectedPage: selectedPage)
    }
    
    private func updatePageControl(selectedPage:Int){
        self.promoImagePageControl.currentPage = selectedPage
    }
    
}

extension DescriptionTableViewCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.promoImageCV.frame.width, height: self.promoImageCV.frame.height)
        
    }
}

extension DescriptionTableViewCell:UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.promoImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! DescriptionImageCollectionViewCell
        if let imageUrl = URL(string: self.promoImages[indexPath.row].imageUrl){
            cell.promoIV.af.setImage(withURL: imageUrl, placeholderImage: UIImage(named: "defaultImage"))
        }
        return cell
    }
    
    
}
