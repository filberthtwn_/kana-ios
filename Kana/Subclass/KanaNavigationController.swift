//
//  KanaNavigationController.swift
//  Kana
//
//  Created by Filbert Hartawan on 15/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class KanaViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = Color.BACKGROUND_PRIMARY
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 17)!]
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        button.titleLabel?.font = UIFont.fontAwesome(ofSize: 21, style: .solid)
        button.setTitleColor(Color.PRIMARY_COLOR, for: .normal)
        button.setTitle(String.fontAwesomeIcon(name: .chevronLeft), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButtonItem
    }
    
    @objc
    private func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
}
