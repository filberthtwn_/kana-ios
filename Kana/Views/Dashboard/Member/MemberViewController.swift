//
//  MemberViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Differentiator
import AlamofireImage
import SVProgressHUD

enum MemberMultipleSectionModel{
    case ShimmerSection(header:String, items:[MemberSectionItem])
    case MembershipSection(header:String, items:[MemberSectionItem])
    case TabLayoutSection(header:String, items:[MemberSectionItem])
    case BenefitSection(header:String, items:[MemberSectionItem])
}

enum MemberSectionItem{
    case ShimmerSectionItem
    case MembershipSection(member:Member?)
    case TabLayoutSection
    case BenefitSection(benefit:Benefit)
}

extension MemberMultipleSectionModel: SectionModelType {
    typealias Item = MemberSectionItem
    
    var items: [MemberSectionItem] {
        switch  self {
        case .ShimmerSection(header: _, items: let items):
            return items.map { $0 }
        case .MembershipSection(header: _, items: let items):
            return items.map { $0 }
        case .TabLayoutSection(header: _, items: let items):
            return items.map { $0 }
        case .BenefitSection(header: _, items: let items):
            return items.map { $0 }
        }
    }
    
    init(original: MemberMultipleSectionModel, items: [Item]) {
        switch original {
        case let .ShimmerSection(header: header, items: _):
            self = .ShimmerSection(header: header, items: items)
        case let .MembershipSection(header: header, items: _):
            self = .MembershipSection(header: header, items: items)
        case let .TabLayoutSection(header: header, items: _):
            self = .TabLayoutSection(header: header, items: items)
        case let .BenefitSection(header, _):
            self = .BenefitSection(header: header, items: items)
        }
    }
}

extension MemberMultipleSectionModel {
    var header: String {
        switch self {
        case .ShimmerSection(header: let title, items: _):
            return title
        case .MembershipSection(header: let title, items: _):
            return title
        case .TabLayoutSection(header: let title, items: _):
            return title
        case .BenefitSection(header: let title, items: _):
            return title
        }
    }
}

struct MemberSection {
    static var MEMBER_CARD = 0
    static var BENEFIT_LIST = 1
}

class MemberViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    private var refreshControl = UIRefreshControl()
    private var isDataLoaded = false
    
    private var member:BehaviorRelay<Member?> = BehaviorRelay(value: nil)
    private var transactions:BehaviorRelay<[Transaction]> = BehaviorRelay(value: [])
    private var benefits:BehaviorRelay<[Benefit]> = BehaviorRelay(value: [])
    private var disposeBag = DisposeBag()
    private var sections:BehaviorRelay<[MemberMultipleSectionModel]> = BehaviorRelay.init(value: [
        .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem
        ])
    ])
    private var dataSource:RxTableViewSectionedReloadDataSource<MemberMultipleSectionModel>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupData()
        self.observeViewModel()
    }
    
    private func setupViews(){
        
        self.tableView
            .rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        self.tableView.register(UINib(nibName: "ShimmerTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        self.tableView.register(UINib(nibName: "MembershipTableViewCell", bundle: nil), forCellReuseIdentifier: "MembershipCardCell")
        self.tableView.register(UINib(nibName: "MemberTabLayoutTableViewCell", bundle: nil), forCellReuseIdentifier: "MemberTabLayoutCell")
        self.tableView.register(UINib(nibName: "BenefitTableViewCell", bundle: nil), forCellReuseIdentifier: "BenefitCell")
        
        self.tableView.register(UINib(nibName: "BenefitTableViewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "BenefitHeader")
        self.tableView.register(UINib(nibName: "BenefitTableViewFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "BenefitFooter")
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.dataSource = RxTableViewSectionedReloadDataSource<MemberMultipleSectionModel>(
            configureCell: { dataSource, tableView, indexPath, item in
                switch dataSource[indexPath] {
                case .ShimmerSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell") as! ShimmerTableViewCell
                    return cell
                case let .MembershipSection(member):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MembershipCardCell", for: indexPath) as! MembershipTableViewCell
                    
                    if let user = UserDefaultHelper.shared.getUser(), let member = member {
                        
                        switch member.tierId.uppercased() {
                        case "JATI":
                            cell.cardIV.image = UIImage(named: "ImageJatiBg")
                        case "MAHONI":
                            cell.cardIV.image = UIImage(named: "ImageMahoniBg")
                        case "PINUS":
                            cell.cardIV.image = UIImage(named: "ImagePinusBg")
                        default:
                            cell.cardIV.image = UIImage(named: "ImageRegularBg")
                        }
                        
                        cell.nameLabel.text = user.name
                        cell.tierLabel.text = member.tierId
                        cell.cardIdLabel.text = member.cardId
                        cell.expireDateLabel.text = "Exp. \(member.expireDate)"
                        cell.signInButton.isHidden = true
                        cell.cardDataView.isHidden = false
                    }else{
                        cell.delegate = self
                        cell.cardDataView.isHidden = true
                    }
                    
                    return cell
                case .TabLayoutSection:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MemberTabLayoutCell") as! MemberTabLayoutTableViewCell
                    
                    cell.benefits = self.benefits.value
                    cell.transactions = self.transactions.value
                    cell.transactionHistoryDelegate = self
                    cell.benefitDelegate = self
                    
                    return cell
                case let .BenefitSection(benefit):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BenefitCell") as! BenefitTableViewCell
                    cell.orderNoLabel.text = "0\(indexPath.item + 1)"
                    cell.titleLabel.text = benefit.description
                    return cell
                }
        }
        )
        
        self.sections
            .bind(to: tableView.rx.items(dataSource: self.dataSource!))
            .disposed(by: disposeBag)
    }
    
    private func setupData(){
        if let user = UserDefaultHelper.shared.getUser(){
            MemberViewModel.shared.getMemberInfo(token: user.token)
        }else{
            MemberViewModel.shared.getBenefits()
        }
    }
    
    private func observeViewModel(){
        MemberViewModel.shared.memberData.asObservable().bind { (member) in
            
            if let member = member{
                self.member.accept(member)
            }
            
        }.disposed(by: self.disposeBag)
        
        TransactionViewModel.shared.transactions.asObservable().bind { (transactions)  in
            
            print("COUNT: \(transactions.count)")
            self.transactions.accept(transactions)
            
        }.disposed(by: self.disposeBag)
        
        MemberViewModel.shared.benefitData.bind { (benefitData) in
            
            self.refreshControl.endRefreshing()
            
            if let benefitData = benefitData{
                
                var sections:[MemberMultipleSectionModel] = []
                
                if UserDefaultHelper.shared.getUser() != nil {
                    
                    let membershipSection:MemberMultipleSectionModel = .MembershipSection(header: "", items: [
                        .MembershipSection(member: self.member.value)
                    ])
                    sections.append(membershipSection)
                    
                    let tabLayoutSection:MemberMultipleSectionModel = .TabLayoutSection(header: "", items:[
                        .TabLayoutSection
                    ])
                    sections.append(tabLayoutSection)
                    
                    if let benefitItem = benefitData.benefitItem{
                        self.benefits.accept(benefitItem.benefits)
                    }
                    
                }else{
                    
                    let membershipSection:MemberMultipleSectionModel = .MembershipSection(header: "", items: [
                        .MembershipSection(member: nil)
                    ])
                    sections.append(membershipSection)
                    
                    var benefitSectionItems:[MemberSectionItem] = []
                    if let benefitItem = benefitData.benefitItem{
                        for benefit in benefitItem.benefits {
                            let benfitSectionItem:MemberSectionItem = .BenefitSection(benefit: benefit)
                            benefitSectionItems.append(benfitSectionItem)
                        }
                        let benefitSection:MemberMultipleSectionModel = .BenefitSection(header: "", items: benefitSectionItems)
                        sections.append(benefitSection)
                    }
                }
                self.isDataLoaded = true
                self.sections.accept(sections)
            }
        }.disposed(by: disposeBag)
        
        self.member.asObservable().bind { (member) in
            if member != nil {
                if let user = UserDefaultHelper.shared.getUser(){
                    TransactionViewModel.shared.getTransactionHistory(customerId: user.id, token: user.token, limit: 3, lastId: nil)
                }
            }
        }.disposed(by: disposeBag)
        
        self.transactions.asObservable().bind { (transactions)  in
            if transactions.count > 0 {
                MemberViewModel.shared.getBenefits()
            }
        }.disposed(by: disposeBag)
        
        MemberViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg{
                
                if errorMsg == ErrorMsg.INVALID_TOKEN{
                    SVProgressHUD.show()
                    self.logoutProcess()
                }
                
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
        
        AuthViewModel.shared.isLogoutSuccess.asObservable().bind { (isSuccess) in
            if isSuccess{                
                SVProgressHUD.showSuccess(withStatus: "Logout Berhasil")
                SVProgressHUD.dismiss(withDelay: Delay.SHORT) {
                    self.logoutProcess()
                }
            }
        }.disposed(by: disposeBag)
        
        AuthViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if errorMsg != nil{
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
    }
    
    private func logoutProcess(){
        UserDefaultHelper.shared.deleteUser()
                
        let navVC = UINavigationController.init(rootViewController: LoginViewController())
        navVC.modalPresentationStyle = .fullScreen
        
        self.present(navVC, animated: true, completion: nil)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.setupData()
    }
    
}

extension MemberViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.isDataLoaded{
            switch section {
            case MemberSection.MEMBER_CARD:
                return .leastNonzeroMagnitude
            default:
                if UserDefaultHelper.shared.getUser() == nil {
                    return UITableView.automaticDimension
                }
                return .leastNonzeroMagnitude
            }
        }else{
            return 16
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.isDataLoaded{
            if UserDefaultHelper.shared.getUser() == nil {
                if section == MemberSection.BENEFIT_LIST{
                    let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BenefitHeader") as! BenefitTableViewHeaderView
                    return header
                }
            }
            return nil
        }else{
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if self.isDataLoaded{
            if UserDefaultHelper.shared.getUser() == nil {
                if section == MemberSection.BENEFIT_LIST{
                    let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BenefitFooter") as! BenefitTableViewFooterView
                    footer.delegate = self
                    if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
                        footer.appVersionLabel.text = "\(appVersion)-\(Network.ENVIRONTMENT)"
                    }
                    footer.myProfileButton.isHidden = true
                    footer.changePasswordButton.isHidden = true
                    footer.notificationButton.isHidden = true
                    
                    return footer
                }
            }
        }
        return nil
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case MemberSection.MEMBER_CARD:
            return .leastNonzeroMagnitude
        default:
            if UserDefaultHelper.shared.getUser() == nil {
                return UITableView.automaticDimension
            }
            return .leastNonzeroMagnitude
        }
    }
}

extension MemberViewController:MembershipCardProtocol{
    func signInButtonAction() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MemberViewController:TransactionHistoryDelegate{
    func didTransactionSelected(salesId: String) {
        let transactionDetailVC = TransactionDetailViewController()
        transactionDetailVC.salesId = salesId
        self.navigationController?.pushViewController(transactionDetailVC, animated: true)
    }
    
    func seeAllHistoryAction() {
        let transactionHistoryVC = TransactionHistoryViewController()
        self.navigationController?.pushViewController(transactionHistoryVC, animated: true)
    }
}

extension MemberViewController:BenefitDelegate{
    func faqAction() {
        let faqVC = faqViewController()
        self.navigationController?.pushViewController(faqVC, animated: true)
    }
    
    func myProfileAction() {
        let myProfileVC = MyProfileViewController()
        self.navigationController?.pushViewController(myProfileVC, animated: true)
    }
    
    func changePasswordAction() {
        let changePasswordVC = ChangePasswordViewController()
        self.navigationController?.pushViewController(changePasswordVC, animated: true)
    }
    
    func notificationAction() {
        
    }
    
    func loginAction() {
        if let user = UserDefaultHelper.shared.getUser(){
            SVProgressHUD.show()
            AuthViewModel.shared.logout(token: user.token)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}
