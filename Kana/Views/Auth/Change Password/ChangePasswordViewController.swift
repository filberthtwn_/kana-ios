//
//  ChangePasswordViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 21/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SVProgressHUD

class ChangePasswordViewController: KanaViewController {
    
    @IBOutlet var oldPasswordTextField: KanaTextField!
    @IBOutlet var newPasswordTextField: KanaTextField!
    @IBOutlet var confirmNewPasswordTextField: KanaTextField!
    
    @IBOutlet var oldPasswordWarningLabel: UILabel!
    @IBOutlet var newPasswordWarningLabel: UILabel!
    @IBOutlet var confirmNewPasswordWarningLabel: UILabel!
    
    @IBOutlet var oldPasswordToggleButton: UIButton!
    @IBOutlet var newPasswordToggleButton: UIButton!
    @IBOutlet var confirmNewPasswordToggleButton: UIButton!
    
    @IBOutlet var changePasswordButton: KanaButton!
    
    private var isOldPasswordValidated = false
    private var isNewPasswordValidated = false
    private var isConfirmNewPasswordValidated = false
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.observeViewModel()
    }
    
    private func setupViews(){
        
        self.hideKeyboardWhenTappedAround()
        
        self.title = "Ganti Password"
        
        self.oldPasswordTextField.kanaTextFieldDegate = self
        self.oldPasswordTextField.removeBorder()
        
        self.newPasswordTextField.kanaTextFieldDegate = self
        self.newPasswordTextField.removeBorder()
        
        self.confirmNewPasswordTextField.kanaTextFieldDegate = self
        self.confirmNewPasswordTextField.removeBorder()
    }
    
    private func observeViewModel(){
        AuthViewModel.shared.isLoginSuccess.asObservable().bind { (isSuccess) in
            if isSuccess{
                SVProgressHUD.showSuccess(withStatus: "Password Berhasil Diubah")
                SVProgressHUD.dismiss(withDelay: Delay.SHORT) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }.disposed(by: disposeBag)
        
        AuthViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if errorMsg != nil{
                SVProgressHUD.showError(withStatus: errorMsg)
               SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
    }
    
    @IBAction func oldPasswordToggleAction(_ sender: Any) {
        // Setup Password Toggle
        if oldPasswordTextField.isSecureTextEntry{
            oldPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eyeSlash), for: .normal)
        }else{
            oldPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eye), for: .normal)
        }
        oldPasswordTextField.isSecureTextEntry = !oldPasswordTextField.isSecureTextEntry
        // END
        
    }
    
    @IBAction func newPasswordToggleAction(_ sender: Any) {
        // Setup Password Toggle
        if newPasswordTextField.isSecureTextEntry{
            newPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eyeSlash), for: .normal)
        }else{
            newPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eye), for: .normal)
        }
        newPasswordTextField.isSecureTextEntry = !newPasswordTextField.isSecureTextEntry
        // END
    }
    
    @IBAction func confirmNewPasswordToggleAction(_ sender: Any) {
        // Setup Password Toggle
        if confirmNewPasswordTextField.isSecureTextEntry{
            confirmNewPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eyeSlash), for: .normal)
        }else{
            confirmNewPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eye), for: .normal)
        }
        confirmNewPasswordTextField.isSecureTextEntry = !confirmNewPasswordTextField.isSecureTextEntry
        // END
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        if let user = UserDefaultHelper.shared.getUser(){
            SVProgressHUD.show()
            AuthViewModel.shared.updatePassword(oldPassword: oldPasswordTextField.text!, newPassword: newPasswordTextField.text!, confirmNewPassword: confirmNewPasswordTextField.text!
                , token: user.token)
        }
    }
}

extension ChangePasswordViewController:UITextFieldDelegate, KanaTextFieldDelegate{
    
    func didValueChanged(sender: UITextField) {
        switch sender {
        case oldPasswordTextField:
            if oldPasswordTextField.text!.count >= 6{
                oldPasswordWarningLabel.isHidden = true
                isOldPasswordValidated = true
            }else{
                oldPasswordWarningLabel.isHidden = false
                isOldPasswordValidated = false
            }
        case newPasswordTextField:
            if newPasswordTextField.text!.count >= 6{
                newPasswordWarningLabel.isHidden = true
                isNewPasswordValidated = true
            }else{
                newPasswordWarningLabel.isHidden = false
                isNewPasswordValidated = false
            }
        default:
            if confirmNewPasswordTextField.text!.count >= 6{
                confirmNewPasswordWarningLabel.isHidden = true
                isConfirmNewPasswordValidated = true
            }else{
                confirmNewPasswordWarningLabel.isHidden = false
                isConfirmNewPasswordValidated = false
            }
        }
        inputValidator()
    }
    
    private func inputValidator(){
        if isOldPasswordValidated, isNewPasswordValidated, isConfirmNewPasswordValidated{
            changePasswordButton.isEnabled = true
            changePasswordButton.backgroundColor = .black
        }else{
            changePasswordButton.isEnabled = false
            changePasswordButton.backgroundColor = Color.PRIMARY_COLOR
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case oldPasswordTextField:
            newPasswordTextField.becomeFirstResponder()
        case newPasswordTextField:
            confirmNewPasswordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}
