//
//  UView+Ext.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func rounded(){
        self.layer.cornerRadius = 8
    }
}
