//
//  PublicAPI.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PublicService {
    static let shared = PublicService()
    private var maxRetryCount = 10
    private var loadCount = 0
    
    func post(url:String, params:[String:Any], completion:@escaping(_ status:String, _ data:Data?,_ error:String?)->Void){
        let stringURL = String(format: "%@%@", arguments: [Network.BASE_URL, url])
        AF.request(stringURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            guard let request = response.request else {return}
            
            print(request)
            print(String(data: request.httpBody!, encoding: .utf8)!)
            print(response.result)
            
            switch response.result{
            case .success:
                    completion(Network.Status.SUCCESS, response.data, nil)
                
            case .failure(let error):
                print(error)
                if error.isSessionTaskError{
                    self.loadCount = self.loadCount + 1
                    if self.loadCount < 10{
                        self.post(url: url, params: params, completion: completion)
                    }else{
                        completion(Network.Status.FAILURE, nil, error.localizedDescription)
                    }
                }else{
                    completion(Network.Status.FAILURE, nil, error.localizedDescription)
               }
            }
        }
    }
    
    func get(url:String, params:[String:Any], completion:@escaping(_ status:String, _ data:Data?,_ error:String?)->Void){
        let stringURL = String(format: "%@%@", arguments: [Network.BASE_URL, url])
        AF.request(stringURL, method: .get, parameters: params, headers: nil).responseJSON { (response) in
            
            guard let request = response.request else {return}
            
            print(request)
            print(response.result)
            
            switch response.result{
            case .success:
                completion(Network.Status.SUCCESS, response.data, nil)
                
            case .failure(let error):
                print(error)
                if error.isSessionTaskError{
                     self.loadCount = self.loadCount + 1
                     if self.loadCount < 10{
                         self.post(url: url, params: params, completion: completion)
                     }else{
                         completion(Network.Status.FAILURE, nil, error.localizedDescription)
                     }
                 }else{
                     completion(Network.Status.FAILURE, nil, error.localizedDescription)
                }
            }
        }
    }
}
