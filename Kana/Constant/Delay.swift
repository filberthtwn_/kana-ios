//
//  Delay.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

enum Delay{
    static let SHORT = TimeInterval(exactly: 1.5)!
    static let LONG = TimeInterval(exactly: 2)!
}
