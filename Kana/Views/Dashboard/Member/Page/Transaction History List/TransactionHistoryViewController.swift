//
//  TransactionHistoryViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 21/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

struct TransactionHistorySection {
    var header:String
    var items:[Transaction]
}

extension TransactionHistorySection:SectionModelType{
    var identity: String {
        return header
    }
    typealias Item = Transaction
    typealias Identity = String
    
    
    init(original: TransactionHistorySection, items: [Transaction]) {
        self = original
        self.items = items
    }
}

class TransactionHistoryViewController: KanaViewController, UITableViewDelegate {
    
    private var refreshControl = UIRefreshControl()
    @IBOutlet var tableView: UITableView!
    
    private var lastId:BehaviorRelay<String> = BehaviorRelay(value: "")
    private var limit:BehaviorRelay<Int> = BehaviorRelay(value: 15)
    private var isMaximum = false
    private var isLoading = false
    
    private var spinner = UIActivityIndicatorView(style: .gray)
    private let disposeBag = DisposeBag()
    private var sections:BehaviorRelay<[TransactionHistorySection]> = BehaviorRelay.init(value: [])
    private var dataSource:RxTableViewSectionedReloadDataSource<TransactionHistorySection>?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.observeViewModel()
    }
    
    private func setupViews(){
        self.title = "History Transaction"
        self.setupTableView()
    }
    
    private func setupTableView(){
        
        self.tableView
        .rx
        .setDelegate(self)
        .disposed(by: disposeBag)
        
        self.tableView.register(UINib(nibName: "HistoryTransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "HistoryTransactionCell")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.dataSource = RxTableViewSectionedReloadDataSource<TransactionHistorySection>(
            configureCell: { dataSource, tableView, indexPath, item in
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTransactionCell") as! HistoryTransactionTableViewCell
                cell.salesIdLabel.text = item.salesId
                cell.transferDateLabel.text = item.transferDate
                cell.receiptIdLabel.text = item.receiptId
                cell.commentLabel.text = item.comment
                cell.amountLabel.text = item.amount
                return cell
                
        })
        
        self.sections
            .bind(to: tableView.rx.items(dataSource: self.dataSource!))
            .disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            let transactionDetailVC = TransactionDetailViewController()
            transactionDetailVC.salesId = self.sections.value[0].items[indexPath.item].salesId
            self.navigationController?.pushViewController(transactionDetailVC, animated: true)
        }).disposed(by: disposeBag)
        
        self.lastId.asObservable()
            .bind { (lastId)  in
                if let user = UserDefaultHelper.shared.getUser(){
                    TransactionViewModel.shared.getTransactionHistory(customerId: user.id, token: user.token, limit: 15, lastId: lastId)
                }
        }.disposed(by: disposeBag)
    }
    
    private func observeViewModel(){
        TransactionViewModel.shared.transactions.asObservable().bind { (transactions) in
            
            self.refreshControl.endRefreshing() 
            
            var sections: [TransactionHistorySection] = []
            let section = TransactionHistorySection(header: "", items: transactions)
            if self.sections.value.count == 0{
                sections.append(section)
                self.sections.accept(sections)
            }else{
                if transactions.count > 0{
                    var update = self.sections.value
                    update[0].items.append(contentsOf: transactions)
                    sections = update
                    self.sections.accept(sections)
                    self.isLoading = false
                }else{
                    self.toggleSpinner(show: false)
                    self.isMaximum = true
                }
            }
        }.disposed(by: disposeBag)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = self.tableView.contentSize.height

        if contentHeight > 0, offsetY >= (contentHeight - self.tableView.frame.size.height){
            if !self.isLoading && !isMaximum{
                self.toggleSpinner(show: true)
                self.lastId.accept(self.sections.value[0].items.last!.salesId)
                self.isLoading = true
            }
        }
    }
    
    // MARK: Toggle Spinner for Infinity Scrolling
    func toggleSpinner(show:Bool){
        if show{
            //MARK: Show TableView Spinner Footer
            self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(50))
            self.tableView.tableFooterView = spinner
            self.spinner.startAnimating()
            // END
        }else{
            //MARK: Hide TableView Spinner Footer
            self.tableView.tableFooterView = nil
            // END
        }
    }
    // END
    
    @objc func refresh(_ sender: AnyObject) {
        self.lastId.accept("")
    }
    
}


//extension TransactionHistoryViewController: UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 2
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTransactionCell") as! HistoryTransactionTableViewCell
//        return cell
//    }
//}
