//
//  AboutMembershipLayoutCollectionViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import SVProgressHUD

class AboutMembershipLayoutCollectionViewCell: UICollectionViewCell {

    @IBOutlet var tableView: UITableView!
    
    var benefits:[Benefit] = []
    
    var delegate:BenefitDelegate?
    private let disposeBag = DisposeBag()
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTableView()
    }
    
    private func setupTableView(){
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "BenefitTableViewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "BenefitHeader")
        self.tableView.register(UINib(nibName: "BenefitTableViewFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "BenefitFooter")
        self.tableView.register(UINib(nibName: "BenefitTableViewCell", bundle: nil), forCellReuseIdentifier: "BenefitCell")
        
    }
}

extension AboutMembershipLayoutCollectionViewCell:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.benefits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BenefitCell", for: indexPath) as! BenefitTableViewCell
        
        cell.orderNoLabel.text = "0\(indexPath.item + 1)"
        cell.titleLabel.text = benefits[indexPath.item].description
        return cell
    }
    
    
}

extension AboutMembershipLayoutCollectionViewCell:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BenefitHeader") as! BenefitTableViewHeaderView
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BenefitFooter") as! BenefitTableViewFooterView
        footer.delegate = self.delegate
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
            footer.appVersionLabel.text = "\(appVersion)-\(Network.ENVIRONTMENT)"
        }
        footer.loginButton.setTitle("Log out", for: .normal)
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         return UITableView.automaticDimension
    }
}
