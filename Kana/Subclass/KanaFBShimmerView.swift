//
//  KanaFBShimmerView.swift
//  Kana
//
//  Created by Filbert Hartawan on 21/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import Shimmer

class KanaFBShimmerView: FBShimmeringView {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews(){
        self.shimmeringSpeed = 1000
        self.isShimmering = true
    }
}
