//
//  PromoDetailViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 14/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SVProgressHUD

enum PromoDetailMultipleSectionModel{
    case ErrorSection(header:String, items:[PromoDetailSectionItem])
    case ShimmerSection(header:String, items:[PromoDetailSectionItem])
    case PromoDescriptionSection(header:String, items:[PromoDetailSectionItem])
    case LocationSection(header:String, items:[PromoDetailSectionItem])
}

enum PromoDetailSectionItem{
    case ErrorSectionItem
    case ShimmerSectionItem
    case DescriptionSection(promo:Promo)
    case LocationSection(stores:Store)
}

extension PromoDetailMultipleSectionModel: SectionModelType {
    typealias Item = PromoDetailSectionItem

    var items: [PromoDetailSectionItem] {
        switch  self {
        case .ErrorSection(header: _, items: let items):
            return items.map { $0 }
        case .ShimmerSection(header: _, items: let items):
            return items.map { $0 }
        case .PromoDescriptionSection(header: _, items: let items):
            return items.map { $0 }
        case .LocationSection(header: _, items: let items):
            return items.map { $0 }
        }
    }

    init(original: PromoDetailMultipleSectionModel, items: [Item]) {
        switch original {
        case let .ErrorSection(header: header, items: _):
           self = .ErrorSection(header: header, items: items)
        case let .ShimmerSection(header: header, items: _):
            self = .ShimmerSection(header: header, items: items)
        case let .PromoDescriptionSection(header: header, items: _):
            self = .PromoDescriptionSection(header: header, items: items)
        case let .LocationSection(header, _):
            self = .LocationSection(header: header, items: items)
        }
    }
}

struct PromoDetailSection {
    static var DESCRIPTION = 0
    static var LOCATION = 1
}

class PromoDetailViewController: KanaViewController {

    @IBOutlet var tableView: UITableView!
    
    private var disposeBag:DisposeBag = DisposeBag()
    private var sections:BehaviorRelay<[PromoDetailMultipleSectionModel]> = BehaviorRelay.init(value: [
        .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem
        ])
    ])
    private var dataSource:RxTableViewSectionedReloadDataSource<PromoDetailMultipleSectionModel>?
    
    var promoId:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Detil Promo "
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disposeBag = DisposeBag()
    }
    
    private func setupTableView(){
        self.tableView
            .rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
        self.tableView.register(UINib(nibName: "ShimmerDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerDetailCell")
        self.tableView.register(UINib(nibName: "DescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "DescriptionCell")
        self.tableView.register(UINib(nibName: "LocationTableViewCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
        self.tableView.register(UINib(nibName: "LocationHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "LocationHeader")
        
        self.dataSource = RxTableViewSectionedReloadDataSource<PromoDetailMultipleSectionModel>(
            configureCell: { dataSource, tableView, indexPath, item in
                switch dataSource[indexPath] {
                case .ShimmerSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerDetailCell") as! ShimmerDetailTableViewCell
                    return cell
                case let .DescriptionSection(promo):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as! DescriptionTableViewCell
                    cell.titleLabel.text = promo.title
                    cell.descriptionLabel.attributedText = promo.content.htmlToAttributedString
                    
                    if promo.imageUrls.count > 0{
                        cell.promoImages = promo.imageUrls
                    }else{
                        var promoImage = PromoImage()
                        promoImage.imageUrl = promo.imageUrl
                        let promoImages:[PromoImage] = [promoImage]
                        
                        cell.promoImages = promoImages
                    }
                    
                    cell.prepareForReuse()
                    cell.promoImageCV.reloadData()
                    
                    return cell
                case let .LocationSection(store):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") as! LocationTableViewCell
                    cell.addressLabel.text = store.name
                    return cell
                case .ErrorSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell") as! ErrorTableViewCell
                    cell.delegate = self
                    return cell
                }
            }
        )
        
        self.sections
            .bind(to: tableView.rx.items(dataSource: self.dataSource!))
            .disposed(by: disposeBag)
    }
    
    private func setupData(){
        PromoViewModel.shared.getPromoDetail(promoId: self.promoId)
        self.observeViewModel()
    }
    
    private func observeViewModel(){
        
        PromoViewModel.shared.promoDetail.bind { (promoData) in
            if let promoData = promoData{
                if let promo = promoData.promo{
                    
                    var sections:[PromoDetailMultipleSectionModel] = []
                    
                    let promoDescSection:PromoDetailMultipleSectionModel = .PromoDescriptionSection(header: "", items: [
                        .DescriptionSection(promo:promo)
                    ])
                    
                    sections.append(promoDescSection)
                    
                    var locationSectionItems:[PromoDetailSectionItem] = []
                    for store in promo.stores{
                        let locationSectionItem:PromoDetailSectionItem = .LocationSection(stores: store)
                        locationSectionItems.append(locationSectionItem)
                    }
                    let locationSection:PromoDetailMultipleSectionModel = .LocationSection(header: "", items: locationSectionItems)
                    sections.append(locationSection)
                    
                    self.sections.accept(sections)
                }
            }
        }.disposed(by: disposeBag)
        
        PromoViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg{
                
                var sections:[PromoDetailMultipleSectionModel] = []
                let section:PromoDetailMultipleSectionModel = .ErrorSection(header: "", items: [
                    .ErrorSectionItem
                ])
                sections.append(section)
                self.sections.accept(sections)
                
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
        
    }
}

extension PromoDetailViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case PromoDetailSection.DESCRIPTION:
            return nil
        default:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader")
            return header
        }
    }
}

extension PromoDetailViewController: ErrorDelegate{
    
    func retryAction() {
        var sections:[PromoDetailMultipleSectionModel] = []
        let shimmer:PromoDetailMultipleSectionModel = .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem
        ])
        sections.append(shimmer)
        self.sections.accept(sections)
        
        PromoViewModel.shared.getPromoDetail(promoId: self.promoId)
    }

}
