//
//  DashboardTableViewHeaderView.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class DashboardTableViewHeaderView: UITableViewHeaderFooterView {
        
    @IBOutlet var backgroundIV: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    //    @IBOutlet var gradientView: UIView!
//    
//    lazy var gradient: CAGradientLayer = {
//        let gradient = CAGradientLayer()
//        gradient.type = .axial
//        gradient.colors = [
//            UIColor.clear.cgColor,
//            UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
//        ]
//        gradient.locations = [0, 1]
//        return gradient
//    }()
    
    override func awakeFromNib() {
        setupViews()
    }
    
    func setupViews(){
//        self.gradientView.layer.addSublayer(gradient)
    }
}
