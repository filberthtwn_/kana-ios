//
//  User.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

class User:NSObject, Codable {
    var id:String = ""
    var email:String = ""
    var name:String = ""
    var birthdate:String = ""
    var phone:String = ""
    var token:String = ""
    
    enum CodingKeys:String, CodingKey {
        case id = "CTM_ID"
        case email = "EMAIL"
        case name = "NAME"
        case birthdate = "BDAY"
        case phone = "PHONE"
        case token = "TOKEN"
    }
    
    required init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.email = try container.decode(String.self, forKey: .email)
        self.name = try container.decode(String.self, forKey: .name)
        self.phone = try container.decode(String.self, forKey: .name)
        
        let birthdate = try container.decode(String.self, forKey: .birthdate)
        
        let dfGet = DateFormatter()
        dfGet.dateFormat = "d/M/yyyy hh:mm:ss a"
        if let dateGet = dfGet.date(from: birthdate){
            let dfPrint = DateFormatter()
            dfPrint.dateFormat = "dd-MM-yyyy"
            self.birthdate = dfPrint.string(from: dateGet)
        }else{
            self.birthdate = birthdate
        }
    
        self.token = try container.decode(String.self, forKey: .token)
    }
}

struct UserData:Codable {
    var customer:User?
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case customer = "CUSTOMER"
        case error = "ERROR"
    }
}

struct UserResponse:Codable{
    var data:UserData?
    
    enum CodingKeys:String, CodingKey {
        case data = "DATA"
    }
}
