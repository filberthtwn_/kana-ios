//
//  NewsDetailViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 15/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SVProgressHUD

enum NewsDetailMultipleSectionModel{
    case ErrorSection(header:String, items:[NewsDetailSectionItem])
    case ShimmerSection(header:String, items:[NewsDetailSectionItem])
    case DescriptionSection(header:String, items:[NewsDetailSectionItem])
    case LocationSection(header:String, items:[NewsDetailSectionItem])
}

enum NewsDetailSectionItem{
    case ErrorSectionItem
    case ShimmerSectionItem
    case DescriptionSection(news:News)
    case LocationSection(stores:Store)
}

extension NewsDetailMultipleSectionModel: SectionModelType {
    typealias Item = NewsDetailSectionItem

    var items: [NewsDetailSectionItem] {
        switch  self {
        case .ErrorSection(header: _, items: let items):
            return items.map { $0 }
        case .ShimmerSection(header: _, items: let items):
            return items.map { $0 }
        case .DescriptionSection(header: _, items: let items):
            return items.map { $0 }
        case .LocationSection(header: _, items: let items):
            return items.map { $0 }
        }
    }

    init(original: NewsDetailMultipleSectionModel, items: [Item]) {
        switch original {
        case let .ErrorSection(header: header, items: _):
           self = .ErrorSection(header: header, items: items)
        case let .ShimmerSection(header: header, items: _):
            self = .ShimmerSection(header: header, items: items)
        case let .DescriptionSection(header: header, items: _):
            self = .DescriptionSection(header: header, items: items)
        case let .LocationSection(header, _):
            self = .LocationSection(header: header, items: items)
        }
    }
}

struct NewsDetailSection {
    static var DESCRIPTION = 0
    static var LOCATION = 1
}

class NewsDetailViewController: KanaViewController {

    @IBOutlet var tableView: UITableView!
    private var isDataLoaded = false
    
    private var disposeBag:DisposeBag = DisposeBag()
    private var sections:BehaviorRelay<[NewsDetailMultipleSectionModel]> = BehaviorRelay.init(value: [
        .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem
        ])
    ])
    private var dataSource:RxTableViewSectionedReloadDataSource<NewsDetailMultipleSectionModel>?
    
    var newsId:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disposeBag = DisposeBag()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Detil News"
    }
    
    private func setupTableView(){
        self.tableView
            .rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
        self.tableView.register(UINib(nibName: "ShimmerDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerDetailCell")
        self.tableView.register(UINib(nibName: "DescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "DescriptionCell")
        self.tableView.register(UINib(nibName: "LocationTableViewCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
        self.tableView.register(UINib(nibName: "LocationHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "LocationHeader")
        
        self.dataSource = RxTableViewSectionedReloadDataSource<NewsDetailMultipleSectionModel>(
            configureCell: { dataSource, tableView, indexPath, item in
                switch dataSource[indexPath] {
                case .ShimmerSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerDetailCell") as! ShimmerDetailTableViewCell
                    return cell
                case let .DescriptionSection(news):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as! DescriptionTableViewCell
                    cell.titleLabel.text = news.title
                    cell.periodLabel.isHidden = true
                    cell.descriptionLabel.attributedText = news.content.htmlToAttributedString
                                        
                    var promoImage = PromoImage()
                    promoImage.imageUrl = news.imageUrls.imageUrl
                    let promoImages:[PromoImage] = [promoImage]

                    cell.promoImages = promoImages
                    cell.promoImagePageControl.isHidden = true

                    cell.prepareForReuse()
                    cell.promoImageCV.reloadData()
                    
                    return cell
                case let .LocationSection(store):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") as! LocationTableViewCell
                    cell.addressLabel.text = store.name
                    return cell
                case .ErrorSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell") as! ErrorTableViewCell
                    cell.delegate = self
                    return cell
                }
            }
        )

        self.sections
            .bind(to: tableView.rx.items(dataSource: self.dataSource!))
            .disposed(by: disposeBag)
    }
    
    private func setupData(){
        NewsViewModel.shared.getNewsDetail(newsId: newsId)
        self.observeViewModel()
    }
    
    private func observeViewModel(){
        
        NewsViewModel.shared.newsDetail.bind { (newsData) in
            if let newsData = newsData{
                
                self.isDataLoaded = true
                
                var sections:[NewsDetailMultipleSectionModel] = []
                if let news = newsData.news{
                    let newsDescSection:NewsDetailMultipleSectionModel = .DescriptionSection(header: "", items: [
                        .DescriptionSection(news: news)
                    ])
                    sections.append(newsDescSection)
                    
                    if news.stores.count != 0 {
                        var locationSectionItems:[NewsDetailSectionItem] = []
                        for store in news.stores{
                            let locationSectionItem:NewsDetailSectionItem = .LocationSection(stores: store)
                            locationSectionItems.append(locationSectionItem)
                        }
                        let locationSection:NewsDetailMultipleSectionModel = .LocationSection(header: "", items: locationSectionItems)
                        sections.append(locationSection)
                    }
                }
                self.sections.accept(sections)
            }
        }.disposed(by: disposeBag)
        
        NewsViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg{
                
                var sections:[NewsDetailMultipleSectionModel] = []
                let section:NewsDetailMultipleSectionModel = .ErrorSection(header: "", items: [
                    .ErrorSectionItem
                ])
                sections.append(section)
                self.sections.accept(sections)
                
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
        
    }
}

extension NewsDetailViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.isDataLoaded{
            switch section {
                case PromoDetailSection.DESCRIPTION:
                    return 0
                default:
                    return UITableView.automaticDimension
            }
        }else{
            return 16
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.isDataLoaded{
            switch section {
                case PromoDetailSection.DESCRIPTION:
                    return nil
                default:
                    let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeaderView
                    header.titleLabel.text = "Lokasi News/Event:"
                    return header
            }
        }else{
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
}

extension NewsDetailViewController: ErrorDelegate{
    
    func retryAction() {
        var sections:[NewsDetailMultipleSectionModel] = []
        let shimmer:NewsDetailMultipleSectionModel = .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem
        ])
        sections.append(shimmer)
        self.sections.accept(sections)
        
        NewsViewModel.shared.getNewsDetail(newsId: newsId)
    }

}
