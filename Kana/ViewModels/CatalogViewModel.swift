//
//  CatalogViewModel.swift
//  Kana
//
//  Created by Filbert Hartawan on 09/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct CatalogViewModel{
    static let shared = CatalogViewModel()
    let catalogData:BehaviorRelay<CatalogData?> = BehaviorRelay(value: nil)
    let isSuccess:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let errorMsg:BehaviorRelay<String?> = BehaviorRelay(value: nil)

    func getCatalog(lastId:String, limit:Int){
        let url = "/getKatalogue"
        let params:[String:Any] = [
            "last_id":lastId,
            "limit":limit
        ]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(CatalogResponse.self, from: data)
                        if let catalogData = response.data{
                            if catalogData.error != ""{
                                self.errorMsg.accept(catalogData.error)
                            }else{
                                self.catalogData.accept(catalogData)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.accept(error.localizedDescription)
                }
            }else{
                self.errorMsg.accept(error)
            }
        }
    }
}
