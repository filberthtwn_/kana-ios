//
//  Promo.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
struct Promo:Decodable {
    var id:String = ""
    var content:String = ""
    var title:String = ""
    var subtitle:String = ""
    var imageUrl:String = ""
    var imageUrls:[PromoImage] = []
    var stores:[Store] = []
    
    enum CodingKeys:String, CodingKey{
        case id = "ID"
        case content = "CONTEN"
        case title = "TITLE"
        case subtitle = "SUBTITLE"
        case imageUrl = "URL_IMAGE"
        case imageUrls = "IMAGE"
        case stores = "STORE"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let id = try container.decodeIfPresent(String.self, forKey: .id){
            self.id = id
        }
        if let content = try container.decodeIfPresent(String.self, forKey: .content){
            self.content = content
        }
        if let title = try container.decodeIfPresent(String.self, forKey: .title){
            self.title = title
        }
        if let subtitle = try container.decodeIfPresent(String.self, forKey: .subtitle){
            self.subtitle = subtitle
        }
        
        if let imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl){
            self.imageUrl.append(imageUrl)
        }
        
        if let imageUrls = try container.decodeIfPresent([PromoImage].self, forKey: .imageUrls){
            self.imageUrls = imageUrls
        }
        
        do {
            if let stores = try container.decodeIfPresent([Store].self, forKey: .stores){
                self.stores = stores
            }
        } catch (let error) {
            print(error)
            if let store = try container.decodeIfPresent(Store.self, forKey: .stores){
                self.stores = [store]
            }
        }
    }
}

struct PromoImage:Decodable{
    var id:String = ""
    var imageUrl:String = ""
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case imageUrl = "URL_IMAGE"
    }
}

struct PromoHighlight:Codable {
    var title:String?
    var subtitle:String = ""
    var imageUrl: String = ""
    
    enum CodingKeys:String, CodingKey {
        case title = "TITLE"
        case subtitle = "SUBTITLE"
        case imageUrl = "IMAGE"
    }
}

struct PromoData:Decodable {
    var highlightPromo:PromoHighlight?
    var promos:[Promo] = []
    var promo:Promo?
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case highlightPromo = "HIGHLIGHTPROMOTION"
        case promos = "Promo"
        case promo = "PROMO"
        case error = "ERROR"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            if let highlightPromo = try container.decodeIfPresent(PromoHighlight?.self, forKey: .highlightPromo){
                self.highlightPromo = highlightPromo
            }
        } catch (let error){
            print(error)
        }
        
        if let promos = try container.decodeIfPresent([Promo].self, forKey: .promos){
            self.promos = promos
        }
        if let promo = try container.decodeIfPresent(Promo.self, forKey: .promo){
            self.promo = promo
        }
        self.error = try container.decode(String.self, forKey: .error)
    }
}

struct PromoResponse:Decodable{
    var data: PromoData?
    
    enum CodingKeys:String, CodingKey {
        case data = "DATA"
    }
}
