//
//  MemberTabLayoutTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

struct MemberTabLayoutTitle {
    static let HISTORY_TRANSACTION = "HISTORY TRANSACTION"
    static let ABOUT_MEMBERSHIP = "ABOUT MEMBERSHIP"
}

class MemberTabLayoutTableViewCell: UITableViewCell {
    
    @IBOutlet var menuBarCollectionView: UICollectionView!
    @IBOutlet var viewPagerCollectionView: UICollectionView!
    @IBOutlet var viewPagerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var horizontalBarLeftConstraint: NSLayoutConstraint!
    
    var benefits:[Benefit] = []
    var transactions:[Transaction] = []
    
    var transactionHistoryDelegate: TransactionHistoryDelegate?
    var benefitDelegate: BenefitDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCollectionView()
    }
    
    private func setupCollectionView(){
                
        self.menuBarCollectionView.delegate = self
        self.menuBarCollectionView.dataSource = self
        
        self.viewPagerCollectionView.delegate = self
        self.viewPagerCollectionView.dataSource = self
        
        self.menuBarCollectionView.register(UINib(nibName: "MenuBarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuBarCell")
        self.viewPagerCollectionView.register(UINib(nibName: "HistoryTransactionLayoutCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HistoryTransactionLayoutCell")
        self.viewPagerCollectionView.register(UINib(nibName: "AboutMembershipLayoutCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AboutMembershipLayoutCell")
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        horizontalBarLeftConstraint.constant = scrollView.contentOffset.x/2
    }
}

extension MemberTabLayoutTableViewCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case menuBarCollectionView:
             return CGSize(width: self.frame.width/2, height: 50)
        default:
            return CGSize(width: self.frame.width, height: 496)
        }
    }
}

extension MemberTabLayoutTableViewCell:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return .zero
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
//
//        return CGSize(width: self.frame.width, height: 50)
//    }
}

extension MemberTabLayoutTableViewCell:UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case menuBarCollectionView:
             return 2
        default:
             return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case menuBarCollectionView:
            self.viewPagerCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
            self.horizontalBarLeftConstraint.constant = CGFloat(indexPath.item) * frame.width/2
            UIView.animate(withDuration: 0.25) {
                self.layoutIfNeeded()
            }
        default:
            break;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case menuBarCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuBarCell", for: indexPath) as! MenuBarCollectionViewCell
            switch indexPath.item {
            case 0:
                cell.titleLabel.text = MemberTabLayoutTitle.HISTORY_TRANSACTION
            default:
                cell.titleLabel.text = MemberTabLayoutTitle.ABOUT_MEMBERSHIP
            }
            return cell
        default:
            switch indexPath.row {
            case 0:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HistoryTransactionLayoutCell", for: indexPath) as! HistoryTransactionLayoutCollectionViewCell
                cell.transactions = self.transactions
                cell.delegate = self.transactionHistoryDelegate
                cell.prepareForReuse()
                return cell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutMembershipLayoutCell", for: indexPath) as! AboutMembershipLayoutCollectionViewCell
                cell.benefits = self.benefits
                cell.delegate = self.benefitDelegate
                
                return cell
            }
        }
    }
    
}

//class DynamicHeightCollectionView: UICollectionView {
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
//            self.invalidateIntrinsicContentSize()
//        }
//    }
//
//    override var intrinsicContentSize: CGSize {
//        return collectionViewLayout.collectionViewContentSize
//    }
//}


