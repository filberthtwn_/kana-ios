//
//  PrivateAPI.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import Alamfore

class PrivateService{
    static let shared = PrivateService()
    
    func post(url:String, params:[String:Any], completion:@escaping(_ data:Data)->Void){
//        refreshToken(destinationURL: url) { (accessToken) in
            let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", accessToken!)]
            let stringURL = String(format: "%@%@", arguments: [Network.shared.getBaseURL(), url])
            print(stringURL)
            AF.request(stringURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                print(response)
                
                switch response.result{
                case .success(let value):
                    let json = JSON(value)
                    if json["error"].string != nil{
                        let error = json["error"].stringValue
                        SVProgressHUD.showError(withStatus: error)
                    }else{
                        completion(response.data!)
                    }
                    
                case .failure(let error):
                    if error.isSessionTaskError{
                        self.get(url: url, params: params, completion: completion)
                    }else{
                       SVProgressHUD.showError(withStatus: error.localizedDescription)
                   }
                }
            }
        }
//    }
}
