//
//  ForgotPasswordViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 22/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SVProgressHUD

class ForgotPasswordViewController: KanaViewController {

    @IBOutlet var emailTextField: KanaTextField!
    @IBOutlet var emailWarningLabel: UILabel!
    @IBOutlet var resetPasswordButton: KanaButton!
    
    private var isEmailValidated = false
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "LUPA PASSWORD"
        
        self.setupViews()
        self.observeViewModel()
    }
    
    private func setupViews(){
        self.hideKeyboardWhenTappedAround()
        self.emailTextField.kanaTextFieldDegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func observeViewModel(){
        AuthViewModel.shared.isForgotPasswordSuccess.bind { (isSuccess) in
            
            SVProgressHUD.showSuccess(withStatus: "Password Reset, please Check Your Email.")
            SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            if isSuccess{
                self.navigationController?.popViewController(animated: true)
            }
        }.disposed(by: self.disposeBag)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func resetPasswordAction(_ sender: Any) {
        SVProgressHUD.show()
        AuthViewModel.shared.forgotPassword(email: emailTextField.text!)
    }
}

extension ForgotPasswordViewController:UITextFieldDelegate, KanaTextFieldDelegate{
    
    func didValueChanged(sender: UITextField) {
        switch sender {
        case emailTextField:
            if emailTextField.isValidEmail(){
                emailWarningLabel.isHidden = true
                isEmailValidated = true
            }else{
                emailWarningLabel.isHidden = false
                isEmailValidated = false
            }
        default:
            break
        }
        inputValidator()
    }
    
    private func inputValidator(){
        if isEmailValidated{
            resetPasswordButton.isEnabled = true
            resetPasswordButton.backgroundColor = .black
        }else{
            resetPasswordButton.isEnabled = false
            resetPasswordButton.backgroundColor = Color.PRIMARY_COLOR
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
