//
//  PromoTableViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Differentiator
import AlamofireImage
import SVProgressHUD

enum PromoMultipleSectionModel{
    case ErrorSection(header:String, items:[PromoSectionItem])
    case ShimmerSection(header:String, items:[PromoSectionItem])
    case PromoSection(header:String, items:[PromoSectionItem])
}

enum PromoSectionItem{
    case ErrorSectionItem
    case ShimmerSectionItem
    case PromoSectionItem(promo:Promo)
}

extension PromoMultipleSectionModel: SectionModelType {
    typealias Item = PromoSectionItem
    
    var items: [PromoSectionItem] {
        switch  self {
        case .ErrorSection(header: _, items: let items):
            return items.map { $0 }
        case .ShimmerSection(header: _, items: let items):
            return items.map { $0 }
        case .PromoSection(header: _, items: let items):
            return items.map { $0 }
        }
    }
    
    init(original: PromoMultipleSectionModel, items: [Item]) {
        switch original {
        case let .ErrorSection(header: header, items: _):
           self = .ErrorSection(header: header, items: items)
        case let .ShimmerSection(header: header, items: _):
            self = .ShimmerSection(header: header, items: items)
        case let .PromoSection(header, _):
            self = .PromoSection(header: header, items: items)
        }
    }
}

class PromoViewController: UIViewController, UNUserNotificationCenterDelegate {
        
    private var refreshControl = UIRefreshControl()
    @IBOutlet var tableView: UITableView!
    
    private var isDataLoaded = false
    
    private var promoData:PromoData? = nil
    private var lastId:BehaviorRelay<String> = BehaviorRelay(value: "")
    private var limit:BehaviorRelay<Int> = BehaviorRelay(value: 50)
    
    private var spinner = UIActivityIndicatorView(style: .gray)
    private let disposeBag = DisposeBag()
    private var sections:BehaviorRelay<[PromoMultipleSectionModel]> = BehaviorRelay.init(value: [
        .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem
        ])
    ])
    private var dataSource:RxTableViewSectionedReloadDataSource<PromoMultipleSectionModel>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupData()
    }
    
    private func setupViews(){
        self.setupTableView()
    }
    
    private func setupData(){
        self.observeViewModel()
    }
    
    private func setupTableView(){
        self.tableView
               .rx
               .setDelegate(self)
               .disposed(by: disposeBag)
            
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
        self.tableView.register(UINib(nibName: "ShimmerTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        self.tableView.register(UINib(nibName: "DashboardTableViewCell", bundle: nil), forCellReuseIdentifier: "DashboardCell")
        self.tableView.register(UINib(nibName: "DashboardTableViewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "DashboardHeader")
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.dataSource = RxTableViewSectionedReloadDataSource<PromoMultipleSectionModel>(
            configureCell: { dataSource, tableView, indexPath, item in
                switch (dataSource[indexPath]){
                case .ShimmerSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell") as! ShimmerTableViewCell
                    return cell
                case let .PromoSectionItem(promo):
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCell") as! DashboardTableViewCell
                    cell.titleLabel.text = "\(promo.title)"
                    cell.descriptionLabel.text = "\(promo.subtitle)"
                    
                    if let imageUrl =  URL(string: promo.imageUrl){
                        cell.cardImageView.af.setImage(withURL: imageUrl, placeholderImage: UIImage(named: "defaultImage"))
                    }
                    
                    let promos = self.sections.value[0].items
                    if indexPath.item == (promos.count-1){
                        cell.separatorLine.isHidden = true
                    }
                    return cell
                case .ErrorSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell") as! ErrorTableViewCell
                    cell.delegate = self
                    return cell
                }
        })
        
        self.sections
            .bind(to: tableView.rx.items(dataSource: self.dataSource!))
            .disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            let promoDetailVC = PromoDetailViewController()
            if let promoData = self.promoData{
                promoDetailVC.promoId = promoData.promos[indexPath.item].id
            }
            self.navigationController?.pushViewController(promoDetailVC, animated: true)
        }).disposed(by: disposeBag)
        
        self.lastId.asObservable()
            .bind { (start)  in
                PromoViewModel.shared.getPromo(lastId: self.lastId.value, limit: self.limit.value)
        }.disposed(by: disposeBag)
    }
    
    private func observeViewModel(){
        
        PromoViewModel.shared.promoData.asObservable().bind { (promoData) in
            
            self.refreshControl.endRefreshing()

            if let promoData = promoData{

                var sections:[PromoMultipleSectionModel] = []

                var promoSectionItems:[PromoSectionItem] = []
                for promo in promoData.promos{
                    let promoItem:PromoSectionItem = .PromoSectionItem(promo: promo)
                    promoSectionItems.append(promoItem)
                }

                let section:PromoMultipleSectionModel = .PromoSection(header: "", items: promoSectionItems)
                sections.append(section)

                self.promoData = promoData
                if self.sections.value.count == 0{
                    self.sections.accept(sections)
                }else{
                    self.isDataLoaded = true
                    self.sections.accept(sections)
//                    var update = self.sections.value
//                    update[0].items.append(contentsOf: promoData.promos)
//                    self.sections.accept(update)
                }
            }
        }.disposed(by: disposeBag)
        
        PromoViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg{
                var sections:[PromoMultipleSectionModel] = []
                let section:PromoMultipleSectionModel = .ErrorSection(header: "", items: [
                    .ErrorSectionItem
                ])
                sections.append(section)
                self.sections.accept(sections)
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.LONG)
            }
        }.disposed(by: disposeBag)
        
    }
    
    #warning("Wait from Backend for Infinite Scrolling")
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = self.tableView.contentSize.height
//
//        if contentHeight > 0, offsetY >= (contentHeight - self.tableView.frame.size.height){
//            self.toggleSpinner(show: true)
//            self.lastId.accept(self.promos.last!.id)
//            if (self.offset + self.limit) < self.totalCount{
//                self.offset += self.limit
//                self.setupData(medicineName: self.medicineName)
//            }
//        }
//    }
    
    // MARK: Toggle Spinner for Infinity Scrolling
    func toggleSpinner(show:Bool){
        if show{
            //MARK: Show TableView Spinner Footer
            self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(50))
            self.tableView.tableFooterView = spinner
            self.spinner.startAnimating()
            // END
        }else{
            //MARK: Hide TableView Spinner Footer
            self.tableView.tableFooterView = nil
            // END
        }
    }
    // END
    
    @objc func refresh(_ sender: AnyObject) {
        self.lastId.accept("")
    }
}

extension PromoViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         if isDataLoaded{
        return self.tableView.frame.height + 16
        }
        return 16
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isDataLoaded{
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "DashboardHeader") as! DashboardTableViewHeaderView
            
            if let promoData = self.promoData, let promoHighlight = promoData.highlightPromo{
                header.titleLabel.text = promoHighlight.title
                if let imageUrl = URL(string: promoHighlight.imageUrl){
                    header.backgroundIV.af.setImage(withURL: imageUrl)
                }
                header.subtitleLabel.text = promoHighlight.subtitle.htmlToString.uppercased()
            }
            
            return header
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
}

extension PromoViewController: ErrorDelegate{
    
    func retryAction() {
        var sections:[PromoMultipleSectionModel] = []
        let shimmer:PromoMultipleSectionModel = .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem
        ])
        sections.append(shimmer)
        self.sections.accept(sections)
        
        self.lastId.accept("")
    }

}
