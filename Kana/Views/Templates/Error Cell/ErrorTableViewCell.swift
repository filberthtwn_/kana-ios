//
//  ErrorTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 22/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

protocol ErrorDelegate {
    func retryAction()
}

class ErrorTableViewCell: UITableViewCell {

    var delegate:ErrorDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func retryAction(_ sender: Any) {
        self.delegate?.retryAction()
    }
}
