//
//  KanaButton.swift
//  Kana
//
//  Created by Filbert Hartawan on 20/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class KanaButton: UIButton {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupViews()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews(){
        self.layer.cornerRadius = 4
    }

}
