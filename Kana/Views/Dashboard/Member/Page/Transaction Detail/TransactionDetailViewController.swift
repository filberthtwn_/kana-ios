//
//  TransactionDetailViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 22/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD

class TransactionDetailViewController: KanaViewController {

    private var refreshControl = UIRefreshControl()
    @IBOutlet var tableView: UITableView!
    
    private var disposeBag = DisposeBag()
    private var salesLines:BehaviorRelay<[SalesLine]> = BehaviorRelay(value: [])
    private var isDataLoaded = false
    
    var salesId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
        self.setupData()
        self.observeViewModel()
    }
    
    private func setupViews(){
        self.title = self.salesId
        self.setupTableView()
    }
    
    private func setupData(){
        if let user = UserDefaultHelper.shared.getUser(){
            TransactionViewModel.shared.getSalesLines(salesId: salesId, token: user.token, limit: 15, lastId: nil)
        }
    }
    
    private func setupTableView(){
        
        self.tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        self.tableView.register(UINib(nibName: "ShimmerTransactionDetailListTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerTransactionDetailListCell")
        self.tableView.register(UINib(nibName: "TransactionDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionDetailCell")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.salesLines.asObservable().bind { (salesLines)  in
            self.tableView.reloadData()
        }.disposed(by: self.disposeBag)
    }
    
    private func observeViewModel(){
        TransactionViewModel.shared.salesLines.asObservable().bind { (salesLines) in
            
            self.refreshControl.endRefreshing()

            self.isDataLoaded = true
            self.salesLines.accept(salesLines)
        }.disposed(by: self.disposeBag)
        
        TransactionViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg{
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: self.disposeBag)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.setupData()
    }

}

extension TransactionDetailViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isDataLoaded{
            return self.salesLines.value.count
        }else{
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isDataLoaded{
           let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionDetailCell", for: indexPath) as! TransactionDetailTableViewCell
            let salesLine = self.salesLines.value[indexPath.row]
            cell.itemIdLabel.text = salesLine.itemId
            cell.nameLabel.text = salesLine.name
            cell.quantityLabel.text = salesLine.quantity
            cell.statusLabel.text = salesLine.status
            
            return cell
        }else{
             let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerTransactionDetailListCell", for: indexPath) as! ShimmerTransactionDetailListTableViewCell
            return cell
        }
    }
}
