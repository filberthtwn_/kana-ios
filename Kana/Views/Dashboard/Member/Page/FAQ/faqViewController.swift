//
//  faqViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 21/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

struct FAQData {
    var isOpened:Bool = false
    var question:String = ""
    var content:FAQDataContent?
}

struct FAQDataContent{
    var answer:String
}

class faqViewController: KanaViewController {

    @IBOutlet var tableView: UITableView!
    
    private var disposeBag = DisposeBag()
    var faqs:[FAQData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupData()
        // Do any additional setup after loading the view.
    }
    
    private func setupViews(){
        self.title = "FAQs"
        self.setupTableView()
    }
    
    private func setupTableView(){
        tableView.register(UINib(nibName: "faqTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "faqTitleCell")
        tableView.register(UINib(nibName: "faqContentTableViewCell", bundle: nil), forCellReuseIdentifier: "faqContentCell")
    }
    
    private func setupData(){
        MemberViewModel.shared.getFaqs()
        self.observeViewModel()
    }
    
    private func observeViewModel(){
        MemberViewModel.shared.faqs.asObservable().bind { (faqs)  in
            
            for faq in faqs{
                let faqData = FAQData(isOpened: false, question: faq.question, content: FAQDataContent(answer: faq.answer))
                self.faqs.append(faqData)
                self.tableView.reloadData()
            }
            
        }.disposed(by: disposeBag)
    }
}

extension faqViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            if faqs[indexPath.section].isOpened == true{
                faqs[indexPath.section].isOpened = false
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }else{
                faqs[indexPath.section].isOpened = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
        }
  
    }
}

extension faqViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return faqs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if faqs[section].isOpened == true{
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "faqTitleCell", for: indexPath) as! faqTitleTableViewCell
            cell.questionLabel.text = faqs[indexPath.section].question
            return cell
        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "faqContentCell", for: indexPath) as! faqContentTableViewCell
            
            if let content = faqs[indexPath.section].content{
                cell.contentLabel.text = content.answer.htmlToString
            }
            
            return cell
        }
    }
}
