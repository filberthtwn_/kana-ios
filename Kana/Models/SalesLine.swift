//
//  SalesLine.swift
//  Kana
//
//  Created by Filbert Hartawan on 22/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

class SalesLine:NSObject, Codable {
    var id:String = ""
    var itemId:String = ""
    var name:String = ""
    var quantity:String = ""
    var status:String = ""
    
    enum CodingKeys:String, CodingKey {
        case id = "LineNum"
        case itemId = "itemId"
        case name = "name"
        case quantity = "qty"
        case status = "status"
    }
}

struct SalesLineData:Decodable {
    var salesLines:[SalesLine] = []
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case salesLines = "SalesLine"
        case error = "ERROR"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            if let salesLines = try container.decodeIfPresent([SalesLine].self, forKey: .salesLines){
                self.salesLines = salesLines
            }
        } catch (let error) {
            print(error)
        }
        
        do {
            if let error = try container.decodeIfPresent(String.self, forKey: .error){
                self.error = error
            }
        } catch (let error) {
            print(error)
        }
    }
}

struct SalesLineResponse:Decodable{
    var data:SalesLineData?
    
    enum CodingKeys:String, CodingKey {
        case data = "SalesLines"
    }
}

