//
//  KanaTextFieldView.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import UIKit

class KanaTextFieldView: UIView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupViews()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews(){
        self.layer.borderWidth = 1
        self.layer.borderColor = Color.BORDER_COLOR
        self.layer.cornerRadius = 4
    }
}

