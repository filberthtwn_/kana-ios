//
//  TransactionViewModel.swift
//  Kana
//
//  Created by Filbert Hartawan on 21/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class TransactionViewModel {
    static let shared = TransactionViewModel()
    let transactions:PublishSubject<[Transaction]> = PublishSubject()
    let salesLines:PublishSubject<[SalesLine]> = PublishSubject()

    let isSuccess:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let errorMsg:BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    func getTransactionHistory(customerId:String, token:String, limit:Int, lastId:String?){
        let url = "/CustAppsSalesOrderHeader"
        var params:[String:Any] = [
            "CustId": customerId,
            "pageLimit": limit,
            "LastSoId":"",
            "token":token
        ]
        
        if let lastId = lastId{
            params["LastSoId"] = lastId
        }
        
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(TransactionResponse.self, from: data)
                        if let transactionData = response.data{
                            if transactionData.error != ""{
                                self.errorMsg.accept(transactionData.error)
                            }else{
                                self.transactions.onNext(transactionData.transactions)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.accept(error.localizedDescription)
                }
            }else{
                self.errorMsg.accept(error)
            }
        }
    }
    
    func getSalesLines(salesId:String, token:String, limit:Int, lastId:String?){
        let url = "/CustAppsSalesOrderDetail"
        var params:[String:Any] = [
            "SalesId": salesId,
            "pageLimit": limit,
            "LastLineNum":"",
            "token":token
        ]
        
        if let lastId = lastId{
            params["LastLineNum"] = lastId
        }
        
        print("ABC")

        
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(SalesLineResponse.self, from: data)
                        if let salesLineData = response.data{
                            if salesLineData.error != ""{
                                self.errorMsg.accept(salesLineData.error)
                            }else{
                                self.salesLines.onNext(salesLineData.salesLines)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.accept(error.localizedDescription)
                }
            }else{
                self.errorMsg.accept(error)
            }
        }
    }
}
