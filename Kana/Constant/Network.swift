//
//  Network.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

enum Network {
    static let ENVIRONTMENT = "DEBUG"
//    static let ENVIRONTMENT = "RELEASE"
    static let BASE_URL = "https://hrm.integragroup-indonesia.com/mbrappsvc.asmx"
    
    enum Status {
        static let SUCCESS = "SUCCESS"
        static let FAILURE = "FAILURE"
    }
}
