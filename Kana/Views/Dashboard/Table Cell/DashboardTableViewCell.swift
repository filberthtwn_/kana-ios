//
//  DashboardTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class DashboardTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var cardImageView: UIImageView!
    
    @IBOutlet var separatorLine: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        self.cardImageView.image = UIImage(named: "defaultImage")
        self.separatorLine.isHidden = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
