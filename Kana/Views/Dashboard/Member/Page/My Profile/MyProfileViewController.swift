//
//  MyProfileViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 21/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class MyProfileViewController: KanaViewController {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var dateOfBirthLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    private func setupViews(){
        
        self.title = "Profil"
        
        if let user = UserDefaultHelper.shared.getUser(){
            self.nameLabel.text = user.name
            self.emailLabel.text = user.email
            self.dateOfBirthLabel.text = user.birthdate
            self.phoneLabel.text = user.phone
        }
    }
}
