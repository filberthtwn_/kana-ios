//
//  MemberViewModel.swift
//  Kana
//
//  Created by Filbert Hartawan on 09/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct MemberViewModel{
    static let shared = MemberViewModel()
    let benefitData:PublishSubject<BenefitData?> = PublishSubject()
    let memberData:PublishSubject<Member?> = PublishSubject()
    let faqs:PublishSubject<[Faq]> = PublishSubject()
    let isSuccess:BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let errorMsg:PublishSubject<String?> = PublishSubject()
    
    func getMemberInfo(token:String){
        let url = "/getMemberInfo"
        let params:[String:Any] = [
            "token":token
        ]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(MemberResponse.self, from: data)
                        if let memberData = response.data{
                            if memberData.error != ""{
                                self.errorMsg.onNext(memberData.error)
                            }else{
                                self.memberData.onNext(memberData.member)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }

    func getBenefits(){
        let url = "/getBenefit"
        let params:[String:Any] = [:]
        PublicService.shared.post(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(BenefitResponse.self, from: data)
                        if let benefitData = response.data{
                            if benefitData.error != ""{
                                self.errorMsg.onNext(benefitData.error)
                            }else{
                                self.benefitData.onNext(benefitData)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
    
    func getFaqs(){
        let url = "/getFaq"
        let params:[String:Any] = [:]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(FaqResponse.self, from: data)
                        if let faqData = response.data{
                            if faqData.error != ""{
                                self.errorMsg.onNext(faqData.error)
                            }else{
                                self.faqs.onNext(faqData.faqs)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
}
