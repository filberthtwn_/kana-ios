//
//  User.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

class Member:NSObject, Codable {
    var cardId:String = ""
    var loyaltyId:String = ""
    var point:String = ""
    var expireDate:String = ""
    var tierId:String = ""

    
    enum CodingKeys:String, CodingKey {
        case cardId = "CARDID"
        case loyaltyId = "LOYALTYID"
        case point = "POINT"
        case expireDate = "EXPIREDDATE"
        case tierId = "TIERID"

    }
    
    required init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.cardId = try container.decode(String.self, forKey: .cardId)
        self.loyaltyId = try container.decode(String.self, forKey: .loyaltyId)
        self.point = try container.decode(String.self, forKey: .point)
        self.expireDate = try container.decode(String.self, forKey: .expireDate)
        self.tierId = try container.decode(String.self, forKey: .tierId)
    }
}

struct MemberData:Codable {
    var member:Member?
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case member = "LOYALTYCARD"
        case error = "ERROR"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            self.member = try container.decode(Member?.self, forKey: .member)
        } catch (let error) {
            print(error)
        }
        self.error = try container.decode(String.self, forKey: .error)
    }
}

struct MemberResponse:Codable{
    var data:MemberData?
    
    enum CodingKeys:String, CodingKey {
        case data = "LOYALTYCARDS"
    }
}
