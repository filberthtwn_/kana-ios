//
//  CatalogViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Differentiator
import AlamofireImage
import SVProgressHUD

enum CatalogMultipleSectionModel{
    case ErrorSection(header:String, items:[CatalogSectionItem])
    case ShimmerSection(header:String, items:[CatalogSectionItem])
    case CatalogSection(header:String, items:[CatalogSectionItem])
}

enum CatalogSectionItem{
    case ErrorSectionItem
    case ShimmerSectionItem
    case CatalogSectionItem(catalog:Catalog)
}

extension CatalogMultipleSectionModel: SectionModelType {
    typealias Item = CatalogSectionItem
    
    var items: [CatalogSectionItem] {
        switch  self {
        case .ErrorSection(header: _, items: let items):
            return items.map { $0 }
        case .ShimmerSection(header: _, items: let items):
            return items.map { $0 }
        case .CatalogSection(header: _, items: let items):
            return items.map { $0 }
        }
    }
    
    init(original: CatalogMultipleSectionModel, items: [Item]) {
        switch original {
        case let .ErrorSection(header: header, items: _):
            self = .ErrorSection(header: header, items: items)
        case let .ShimmerSection(header: header, items: _):
            self = .ShimmerSection(header: header, items: items)
        case let .CatalogSection(header, _):
            self = .CatalogSection(header: header, items: items)
        }
    }
}

class CatalogViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    private var isDataLoaded = false
    private var refreshControl = UIRefreshControl()
    
    private var catalogData:CatalogData? = nil
    private var lastId:BehaviorRelay<String> = BehaviorRelay(value: "")
    private var limit:BehaviorRelay<Int> = BehaviorRelay(value: 50)
    
    private let disposeBag = DisposeBag()
    private var sections:BehaviorRelay<[CatalogMultipleSectionModel]> = BehaviorRelay.init(value: [
        .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem
        ])
    ])
    private var dataSource:RxTableViewSectionedReloadDataSource<CatalogMultipleSectionModel>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.observeViewModel()
    }
    
    private func setupViews(){
        self.setupTableView()
    }
    
    private func setupTableView(){
        self.tableView
            .rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
        self.tableView.register(UINib(nibName: "ShimmerTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        self.tableView.register(UINib(nibName: "CatalogTableViewCell", bundle: nil), forCellReuseIdentifier: "CatalogCell")
        self.tableView.register(UINib(nibName: "DashboardTableViewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "DashboardHeader")
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.dataSource = RxTableViewSectionedReloadDataSource<CatalogMultipleSectionModel>(
            configureCell: { dataSource, tableView, indexPath, item in
                
                switch (dataSource[indexPath]){
                case .ShimmerSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell") as! ShimmerTableViewCell
                    return cell
                case let .CatalogSectionItem(catalog):
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CatalogCell") as! CatalogTableViewCell
                    cell.titleLabel.text = catalog.title.capitalizingFirstLetter()
                    cell.descriptionLabel.text = catalog.subtitle
                    
                    if let imageUrl =  URL(string: catalog.imageUrl){
                        cell.cardImageView.af.setImage(withURL: imageUrl, placeholderImage: UIImage(named: "defaultImage"))
                    }
                    
                    if let catalogData = self.catalogData{
                        if indexPath.item >= (catalogData.catalogs.count-1){
                            cell.separatorLine.isHidden = true
                        }else{
                            cell.separatorLine.isHidden = false
                        }
                    }
                    
                    return cell
                    
                case .ErrorSectionItem:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell") as! ErrorTableViewCell
                    cell.delegate = self
                    return cell
                }
            })
        
        self.sections
            .bind(to: tableView.rx.items(dataSource: self.dataSource!))
            .disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            let catalogDetailVC = CatalogDetailViewController()
            if let catalogData = self.catalogData{
                catalogDetailVC.pdfUrl = catalogData.catalogs[indexPath.row].pdfUrl
            }
            self.navigationController?.pushViewController(catalogDetailVC, animated: true)
        }).disposed(by: disposeBag)
        
        self.lastId.asObservable()
            .bind { (start)  in
                CatalogViewModel.shared.getCatalog(lastId: "", limit: 50)
            }.disposed(by: disposeBag)
    }
    
    private func observeViewModel(){
        
        CatalogViewModel.shared.catalogData.asObservable().bind { (catalogData) in
            
            self.refreshControl.endRefreshing()
            
            if let catalogData = catalogData{
                
                var sections:[CatalogMultipleSectionModel] = []
                
                var catalogSectionItems:[CatalogSectionItem] = []
                for catalog in catalogData.catalogs{
                    let catalogItem:CatalogSectionItem = .CatalogSectionItem(catalog: catalog)
                    catalogSectionItems.append(catalogItem)
                }
                
                let section:CatalogMultipleSectionModel = .CatalogSection(header: "", items: catalogSectionItems)
                sections.append(section)
                
                self.catalogData = catalogData
                if self.sections.value.count == 0{
                    self.sections.accept(sections)
                }else{
                    self.isDataLoaded = true
                    self.sections.accept(sections)
                    //                    var update = self.sections.value
                    //                    update[0].items.append(contentsOf: promoData.promos)
                    //                    self.sections.accept(update)
                }
            }
        }.disposed(by: disposeBag)
        
        CatalogViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg{
                
                var sections:[CatalogMultipleSectionModel] = []
                let section:CatalogMultipleSectionModel = .ErrorSection(header: "", items: [
                    .ErrorSectionItem
                ])
                sections.append(section)
                self.sections.accept(sections)
                
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
        
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.lastId.accept("")
    }
}

extension CatalogViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if  self.isDataLoaded{
            return self.tableView.frame.height + 16
        }else{
            return 16
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.isDataLoaded{
            
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "DashboardHeader") as! DashboardTableViewHeaderView
            
            if let catalogData = self.catalogData, let catalogHighlight = catalogData.catalogHighlight{
                header.titleLabel.text = catalogHighlight.title
                if let imageUrl = URL(string: catalogHighlight.imageUrl){
                    header.backgroundIV.af.setImage(withURL: imageUrl)
                }
                header.subtitleLabel.text = catalogHighlight.subtitle.htmlToString.uppercased()
            }
            return header
            
        }else{
            
            return UIView()
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
}

extension CatalogViewController: ErrorDelegate{
    
    func retryAction() {
        var sections:[CatalogMultipleSectionModel] = []
        let shimmer:CatalogMultipleSectionModel = .ShimmerSection(header: "shimmer", items: [
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem,
            .ShimmerSectionItem
        ])
        sections.append(shimmer)
        self.sections.accept(sections)
        
        self.lastId.accept("")
    }
    
}
