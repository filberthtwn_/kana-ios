//
//  StoreDetailViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 15/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SVProgressHUD
import MapKit

class StoreDetailViewController: KanaViewController {

    @IBOutlet var storeIV: UIImageView!
    
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var whatsappNumberLabel: UILabel!
    @IBOutlet var workingTimeLabel: UILabel!
    @IBOutlet var facilityLabel: UILabel!

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var galleryCollectionView: UICollectionView!
    
    private var isError:BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    private var disposeBag = DisposeBag()
    private var store:Store?
    var storeId:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        
        self.setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disposeBag = DisposeBag()
    }
    
    private func setupViews(){
        
        self.galleryCollectionView.register(UINib(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCell")
        self.setupTableView()
    }
    
    private func setupMapView(title:String, latitude:Double, longitude:Double){
        let annotations = MKPointAnnotation()
        annotations.title = title
        annotations.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let distance = CLLocationDistance(exactly: 1000)!
        let region = MKCoordinateRegion( center: annotations.coordinate, latitudinalMeters: distance, longitudinalMeters: distance)
        mapView.setRegion(mapView.regionThatFits(region), animated: true)
        mapView.addAnnotation(annotations)
    }
    
    private func setupTableView(){
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
        self.tableView.register(UINib(nibName: "ShimmerDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerDetailCell")
    }
    
    private func setupData(){
        StoreViewModel.shared.getStoreDetail(storeId: storeId)
        self.observeViewModel()
    }
    
    private func observeViewModel(){
        StoreViewModel.shared.storeData.bind { (storeData) in
            if let storeData = storeData{
                if let store = storeData.store{
                    self.store = store
                    
                    if let imageUrl =  URL(string: store.imageUrl){
                        self.storeIV.af.setImage(withURL: imageUrl, placeholderImage: UIImage(named: "defaultImage"))
                    }
                    
                    self.title = store.name
                    self.addressLabel.text = store.address
                    self.phoneNumberLabel.text = store.phone
                    self.whatsappNumberLabel.text = store.whatsapp
                    self.workingTimeLabel.text = store.workingTime
                    self.facilityLabel.text = store.facility
                    
                    self.setupMapView(title: store.name, latitude: store.latitude, longitude: store.longitude)
                }
            }
            self.scrollView.isHidden = false
            self.tableView.isHidden = true
        }.disposed(by: disposeBag)
        
        StoreViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            if let errorMsg = errorMsg{
                
                self.isError.accept(true)
                
                SVProgressHUD.showError(withStatus: errorMsg)
                SVProgressHUD.dismiss(withDelay: Delay.SHORT)
            }
        }.disposed(by: disposeBag)
        
        self.isError.asObservable().bind { (isError) in
            self.tableView.reloadData()
        }.disposed(by: disposeBag)
        
    }
    
    @IBAction func whatsappAction(_ sender: Any) {
        if let store = self.store{
            if let url = URL(string: "https://wa.me/\(store.whatsapp.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: ""))") {
                print(url)
                UIApplication.shared.open(url)
            }
        }
    }
    
    @IBAction func callAction(_ sender: Any) {
        if let store = self.store{
            if let url = URL(string: "tel://\(store.phone.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "(", with: ""))") {
                UIApplication.shared.open(url)
            }
        }
        
    }
}

extension StoreDetailViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath)
        return cell
    }
}

extension StoreDetailViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isError.value{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell") as! ErrorTableViewCell
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerDetailCell", for: indexPath) as! ShimmerDetailTableViewCell
            return cell
        }
    }
    
    
}

extension StoreDetailViewController: ErrorDelegate{
    
    func retryAction() {
        self.tableView.isHidden = false
        self.isError.accept(false)
        StoreViewModel.shared.getStoreDetail(storeId: storeId)
    }

}
