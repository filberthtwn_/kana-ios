//
//  Faq.swift
//  Kana
//
//  Created by Filbert Hartawan on 21/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

class Faq:NSObject, Codable {
    var id:String = ""
    var question:String = ""
    var answer:String = ""
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case question = "QUESTION"
        case answer = "ANSWER"
    }
}

struct FaqData:Decodable {
    var faqs:[Faq] = []
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case faqs = "FAQ"
        case error = "ERROR"
    }
}

struct FaqResponse:Decodable{
    var data:FaqData?
    
    enum CodingKeys:String, CodingKey {
        case data = "DATA"
    }
}
