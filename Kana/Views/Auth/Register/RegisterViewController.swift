//
//  RegisterViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class FirstRegisterViewController: UIViewController {

    @IBOutlet var emailTextField: KanaTextField!
    @IBOutlet var passwordTextField: KanaTextField!
    @IBOutlet var cPasswordTextField: KanaTextField!
    @IBOutlet var phoneTextField: KanaTextField!
    
    @IBOutlet var passwordToggleButton: UIButton!
    @IBOutlet var cPasswordToggleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        hideKeyboardWhenTappedAround()
    }
    
    private func pushToSecondRegister(){
//        self.navigationController?.pushViewController(<#T##viewController: UIViewController##UIViewController#>, animated: <#T##Bool#>)
    }
    
    @IBAction func passwordToggleAction(_ sender: Any) {
        // Setup Password Toggle
        if passwordTextField.isSecureTextEntry{
            passwordToggleButton.setTitle(String.fontAwesomeIcon(name: .eyeSlash), for: .normal)
        }else{
            passwordToggleButton.setTitle(String.fontAwesomeIcon(name: .eye), for: .normal)
        }
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        // END
    }
    
    @IBAction func cPasswordToggleAction(_ sender: Any) {
        // Setup Password Toggle
        if cPasswordTextField.isSecureTextEntry{
            cPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eyeSlash), for: .normal)
        }else{
            cPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eye), for: .normal)
        }
        cPasswordTextField.isSecureTextEntry = !cPasswordTextField.isSecureTextEntry
        // END
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
    }
}

extension FirstRegisterViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            cPasswordTextField.becomeFirstResponder()
        case cPasswordTextField:
            phoneTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
//            loginProcess()
        }
        return true
    }
}
