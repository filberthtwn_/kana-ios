//
//  ContactTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet var instagramLabel: UILabel!
    @IBOutlet var facebookLabel: UILabel!
    @IBOutlet var youtubeLabel: UILabel!
    @IBOutlet var websiteLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func instagramAction(_ sender: Any) {
        if let url = URL(string: "instagram://user?username=kana.furniture") {
            UIApplication.shared.open(url)
        }else{
            if let webUrl = URL(string: "https://www.instagram.com/kana.furniture"){
                UIApplication.shared.open(webUrl)
            }
        }
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        if let url = URL(string: "fb://profile?id=kanafurniture") {
            UIApplication.shared.open(url, options: [:]) { (isSuccess) in
                if (!isSuccess){
                    if let webUrl = URL(string: "https://m.facebook.com/kanafurniture"){
                        UIApplication.shared.open(webUrl)
                    }
                }
            }
        }
    }
    
    @IBAction func youtubeAction(_ sender: Any) {
        if let url = URL(string: "https://www.youtube.com/channel/UCGI3PzbVun-S5NImMz8VSPQ") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func websiteAction(_ sender: Any) {
        if let url = URL(string: "https://www.kanafurniture.com/") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func emailAction(_ sender: Any) {
        if let url = URL(string: "mailto:ask@kanafurniture.com") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
}
