//
//  TabBarViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import SVProgressHUD

class TitleImageView: UIImageView{
    override var intrinsicContentSize: CGSize {
      return UIView.layoutFittingExpandedSize
    }

}

class TabBarViewController: KanaViewController, UNUserNotificationCenterDelegate {

//    @IBOutlet var contentView: UIView!
    @IBOutlet var mainCV: UICollectionView!
    
    @IBOutlet var tabBarIcons:[UIImageView]!
    @IBOutlet var tabBarLabels: [UILabel]!
    
    private var viewControllers: [UIViewController]!
    
    private var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNotificationPermission()
        self.setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func setupViews(){
        
        self.setupNavigationBar()
        
        self.viewControllers = [PromoViewController(), NewsViewController(), StoreViewController(), CatalogViewController(), MemberViewController()]
        let selectedVC = viewControllers[selectedIndex]
        addChild(selectedVC)
        
        self.moveView(currentIndex: 0)
        self.setupCollectionView()
    }
    
    private func setupCollectionView(){
        self.mainCV.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
    }
    
    private func setupNotificationPermission(){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
    }
    
    private func setupNavigationBar(){
        
        // MARK: SETUP NAVIGATION HEADER
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width * 0.35, height: 40))
        
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0, width: customView.frame.size.width, height: customView.frame.size.height)
        imageView.image = UIImage(named: "ImageKana")
        imageView.contentMode = .scaleAspectFit
        
        customView.addSubview(imageView)
        self.navigationItem.titleView = customView
        // END
        
        // MARK: SETUP NOTIFICATION BUTTON
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        button.titleLabel?.font = UIFont.fontAwesome(ofSize: 17, style: .regular)
        button.setTitleColor(.black, for: .normal)
        button.setTitle(String.fontAwesomeIcon(name: .bell), for: .normal)
        button.addTarget(self, action: #selector(notificationAction(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButtonItem
        // END
        
        self.navigationItem.leftBarButtonItem = .none
    }
    
    @IBAction func didPressTab(_ sender: UIButton) {
        self.mainCV.selectItem(at: IndexPath(row: sender.tag, section: 0), animated: true, scrollPosition: .left)
        self.moveView(currentIndex: sender.tag)
    }
    
    private func moveView(currentIndex:Int){
        let previousIndex = selectedIndex
        tabBarLabels[previousIndex].textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 0.5)
        tabBarIcons[previousIndex].tintColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 0.5)
        
        self.selectedIndex = currentIndex
        tabBarLabels[selectedIndex].textColor = .black
        tabBarIcons[selectedIndex].tintColor = .black
    }

    @objc func notificationAction(_ sender: Any) {
        if UserDefaultHelper.shared.getUser() != nil{
            let notificationVC = NotificationsViewController()
            self.navigationController?.pushViewController(notificationVC, animated: true)
        }else{
            SVProgressHUD.showInfo(withStatus: "Anda harus login untuk mengakses fitur ini.")
            SVProgressHUD.dismiss(withDelay: Delay.SHORT)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let selectedPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
        self.moveView(currentIndex: selectedPage)
    }

}

extension TabBarViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.mainCV.frame.width, height: self.mainCV.frame.height)
    }
}

extension TabBarViewController:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewControllers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let viewController = self.viewControllers[indexPath.row]
        viewController.view.frame = self.mainCV.frame
        cell.addSubview(viewController.view)
        
        return cell
    }
    
}
