//
//  PromoLocationTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 14/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet var addressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addressLabel.sizeToFit()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
