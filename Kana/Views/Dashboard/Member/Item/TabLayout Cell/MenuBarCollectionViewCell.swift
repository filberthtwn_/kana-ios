//
//  MenuBarCollectionViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class MenuBarCollectionViewCell: UICollectionViewCell {

    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
