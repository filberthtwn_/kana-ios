//
//  Transactions.swift
//  Kana
//
//  Created by Filbert Hartawan on 21/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

class Transaction:NSObject, Codable {
    var salesId:String = ""
    var receiptId:String = ""
    var transactionId:String = ""
    var invoiceId:String = ""
    var customerName:String = ""
    var transferDate:String = ""
    var amount:String = ""
    var comment:String = ""
    
    enum CodingKeys:String, CodingKey {
        case salesId = "SalesId"
        case receiptId = "receiptId"
        case transactionId = "transactionId"
        case invoiceId = "invoiceId"
        case customerName = "CustName"
        case transferDate = "transDate"
        case amount = "amount"
        case comment = "comment"
    }
}

struct TransactionData:Decodable {
    var transactions:[Transaction] = []
    var transaction:Transaction?
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case transactions = "SO"
        case error = "ERROR"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            if let transactions = try container.decodeIfPresent([Transaction].self, forKey: .transactions){
                self.transactions = transactions
            }
        } catch {
            let transaction = try container.decode(Transaction.self, forKey: .transactions)
            self.transactions.append(transaction)
        }
       
        if let error = try container.decodeIfPresent(String.self, forKey: .error){
            self.error = error
        }
    }
}

struct TransactionResponse:Decodable{
    var data:TransactionData?
    
    enum CodingKeys:String, CodingKey {
        case data = "SalesOrder"
    }
}
