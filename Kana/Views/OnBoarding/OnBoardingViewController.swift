//
//  OnBoardingViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 22/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

struct OnBoardingItem{
    var title:String = ""
    var subtitle:String = ""
    var background:UIImage? = nil
}

class OnBoardingViewController: UIViewController {

    @IBOutlet var logoImageView: UIImageView!
//    @IBOutlet var pageControl: KanaPageControl!
    @IBOutlet var pageControlDots:[UIImageView]!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var getStartedButton: UIButton!
    
    private let items:[OnBoardingItem] = [
        OnBoardingItem(title: "Hi!",
                       subtitle: "Selamat datang di KANA Furniture, lifestyle store untuk furniture dan home decor.",
                       background: UIImage(named: "OnBoardingFirst")),
        OnBoardingItem(title: "Furnish your story",
                       subtitle: "Menjadikan setiap ruang sebagai ekspresi dan memori bergulirnya cerita indah.",
                       background: UIImage(named: "OnBoardingSecond")),
        OnBoardingItem(title: "Kana Membership",
                       subtitle: "Jalin cerita indahmu disini, lewat kemudahan, layanan dan ragam benefit KANA membership.",
                       background: UIImage(named: "OnBoardingThird")),
        OnBoardingItem(title: "Prioritas",
                       subtitle: "KANA tahu kalau semua pasti senang dapat promo. Kamulah yang pertama dan tercepat dapat infonya.",
                       background: UIImage(named: "OnBoardingForth")),
        OnBoardingItem(title: "Up to date",
                       subtitle: "Jangan kuatir ketinggalan event-event menarik, karena KANA pasti infoin kamu yang pertama.",
                       background: UIImage(named: "OnBoardingFifth")),
        OnBoardingItem(title: "Benefits",
                       subtitle: "Dengan Aplikasi KANA Membership semua kemudahan siap untuk kamu nikmati. Segera register dan aktivasi membership dengan datang ke semua store KANA.",
                       background: UIImage(named: "OnBoardingSixth"))
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.pageControl.safeAreaInsets =
//        self.pageControl.
//        self.pageControl.frame = CGRect(x: self.pageControl.frame.origin.x, y: self.pageControl.frame.origin.y, width: 100, height: 50);

//        self.pageControl.updateDots()
        self.setupKanaLogo()
    }
    
    private func setupKanaLogo(){
        let width = self.view.frame.width/2
        let height = width/2
        
        self.logoImageView.frame = CGRect(x: (self.view.frame.width-width)/2, y: (self.view.frame.height-height )/2, width: width, height: height)
        
        let newWidth = self.view.frame.width*0.25
        let newHeight = width/2
        
        UIView.animate(withDuration: 1) {
            self.logoImageView.frame = CGRect(x: 16, y: (self.view.frame.height - newHeight) - 16, width: newWidth, height: newHeight)
            self.view.layoutIfNeeded()
        }
    }
    
    private func setupCollectionView(){
        self.collectionView.register(UINib(nibName: "OnBoardingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OnBoardingCell")
    }
    
    @IBAction func skipAction(_ sender: Any) {
        self.updatePageControl(selectedPage: (self.pageControlDots.count-1))
        self.skipButton.isHidden = true
        self.getStartedButton.isHidden = false
        self.collectionView.scrollToItem(at: IndexPath(row: self.items.count-1, section: 0), at: .left, animated: true)
    }
    
    @IBAction func getStartedAction(_ sender: Any) {
        UserDefaultHelper.shared.setOnBoardingLoaded(isLoaded: true)
        
        let navigationVC = UINavigationController.init(rootViewController: LoginViewController())
        navigationVC.modalPresentationStyle = .fullScreen
        self.present(navigationVC, animated: true, completion: nil)
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let selectedPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
        self.updatePageControl(selectedPage: selectedPage)
        
        if selectedPage > 0{
            if selectedPage == (self.items.count - 1){
                self.skipButton.isHidden = true
                self.getStartedButton.isHidden = false
            }else{
                self.getStartedButton.isHidden = true
                self.skipButton.isHidden = true
            }
        }else{
            self.getStartedButton.isHidden = true
            self.skipButton.isHidden = false
        }
    }
    
    private func updatePageControl(selectedPage:Int){
        for pageControlDot in self.pageControlDots  {
            pageControlDot.image = UIImage(named: "PageControlNotSelectedIcon")
        }
        self.pageControlDots[selectedPage].image = UIImage(named: "PageControlSelectedIcon")
    }
    
}

extension OnBoardingViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height)
    }
}


extension OnBoardingViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardingCell", for: indexPath) as! OnBoardingCollectionViewCell
        cell.backgroundImageView.image = items[indexPath.row].background
        cell.titleLabel.text = items[indexPath.row].title
        cell.subtitleLabel.text = items[indexPath.row].subtitle
        
        return cell
    }
    
    
}
