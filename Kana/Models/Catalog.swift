//
//  Catalog.swift
//  Kana
//
//  Created by Filbert Hartawan on 09/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

struct Catalog:Codable {
    var id:String = ""
    var title:String = ""
    var subtitle:String = ""
    var imageUrl:String = ""
    var pdfUrl:String = ""
    
    enum CodingKeys:String, CodingKey{
        case id = "ID"
        case title = "TITLE"
        case subtitle = "SUBTITLE"
        case pdfUrl = "PDF_URL"
        case imageUrl = "IMAGE_URL"
    }
}

struct CatalogHighlight:Codable {
    var title:String?
    var subtitle:String = ""
    var imageUrl: String = ""
    
    enum CodingKeys:String, CodingKey {
        case title = "TITLE"
        case subtitle = "SUBTITLE"
        case imageUrl = "IMAGE_COVER"
    }
}

struct CatalogData:Codable {
    var catalogHighlight:CatalogHighlight?
    var catalogs:[Catalog] = []
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case catalogHighlight = "HIGHLIGHTKATALOGUES"
        case catalogs = "KATALOGUES"
        case error = "ERROR"
    }
}

struct CatalogResponse:Codable{
    var data: CatalogData?
    
    enum CodingKeys:String, CodingKey {
        case data = "DATA"
    }
}
