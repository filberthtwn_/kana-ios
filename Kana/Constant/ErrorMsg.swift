//
//  ErrorMsg.swift
//  Kana
//
//  Created by Filbert Hartawan on 22/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

enum ErrorMsg{
    static let INVALID_TOKEN = "InvalidToken"
}
