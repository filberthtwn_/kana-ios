//
//  BenefitTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 02/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class BenefitTableViewCell: UITableViewCell {
    
    @IBOutlet var orderNoLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
