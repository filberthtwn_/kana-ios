//
//  NewsViewModel.swift
//  Kana
//
//  Created by Filbert Hartawan on 09/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

struct NewsViewModel{
    static let shared = NewsViewModel()
    let promoHighlight:BehaviorRelay<PromoHighlight?> = BehaviorRelay(value: nil)
    let newsData:PublishSubject<NewsData?> = PublishSubject()
    let newsDetail:PublishSubject<NewsData?> = PublishSubject()
    let isSuccess:PublishSubject<Bool> = PublishSubject()
    let errorMsg:PublishSubject<String?> = PublishSubject()

    func getNews(lastId:String, limit:Int){
        let url = "/getNews"
        let params:[String:Any] = [
            "last_id":lastId,
            "limit":limit,
            "highlight":""
        ]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(NewsResponse.self, from: data)
                        if let newsData = response.data{
                            if newsData.error != ""{
                                self.errorMsg.onNext(newsData.error)
                            }else{
                                self.newsData.onNext(newsData)
                            }
                        }
                    }
                }catch (let error){
                    print(error)
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
    
    func getNewsDetail(newsId:String){
        let url = "/getNewsDetail"
        let params:[String:Any] = [
            "id": newsId
        ]
        PublicService.shared.get(url: url, params: params) { (status, data, error) -> (Void) in
            if status == Network.Status.SUCCESS{
                do{
                    if let data = data{
                        let response = try JSONDecoder().decode(NewsResponse.self, from: data)
                        if let newsData = response.data{
                            if newsData.error != ""{
                                self.errorMsg.onNext(newsData.error)
                            }else{
                                self.newsDetail.onNext(newsData)
                            }
                        }
                    }
                }catch (let error){
                    self.errorMsg.onNext(error.localizedDescription)
                }
            }else{
                self.errorMsg.onNext(error)
            }
        }
    }
}
