//
//  KanaGradientView.swift
//  Kana
//
//  Created by Filbert Hartawan on 02/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class KanaGradientView: UIView {
    
    override func prepareForInterfaceBuilder() {
        self.gradient.frame = self.bounds
        self.layer.addSublayer(gradient)
    }
    
    lazy var gradient: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.type = .axial
        gradient.colors = [
            UIColor.clear.cgColor,
            UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
        ]
        gradient.locations = [0, 1]
        return gradient
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.gradient.frame = self.bounds
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupViews()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews(){
        self.updateConstraints()

        self.gradient.frame = self.bounds
        self.layer.addSublayer(gradient)
    }
}
