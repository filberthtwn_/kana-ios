//
//  StoreTableViewFooterView.swift
//  Kana
//
//  Created by Filbert Hartawan on 01/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class StoreTableViewFooterView: UITableViewHeaderFooterView {
    
    var dekorumaLink:String = ""
    var tokopediaLink:String =  ""
    var websiteLink:String = ""
    
    @IBAction func dekorumaAction(_ sender: Any) {
//        if let url = URL(string: "dekoruma://brands=kana-furniture") {
//            UIApplication.shared.open(url)
//        }else{
//            if let webUrl = URL(string: "https://m.dekoruma.com/brands/KANA-FURNITURE"){
//                UIApplication.shared.open(webUrl)
//            }
//        }
        if let url = URL(string: dekorumaLink) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func tokopediaAction(_ sender: Any) {
        if let url = URL(string: tokopediaLink) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func kanaAction(_ sender: Any) {
        if let url = URL(string: websiteLink) {
            UIApplication.shared.open(url)
        }
    }
    
}
