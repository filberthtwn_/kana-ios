//
//  UserDefaultHelper.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

struct UserDefaultHelper {
    static let shared = UserDefaultHelper()
    
    func setOnBoardingLoaded(isLoaded:Bool){
        let userDefaults = UserDefaults.standard
        userDefaults.set(isLoaded, forKey: "isOnBoardingLoaded")
        userDefaults.synchronize()
    }
    
    func getisOnBoardingLoaded()->Bool{
        return UserDefaults.standard.bool(forKey: "isOnBoardingLoaded")
    }
    
    func setupUser(data:Data){
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey: "current_user")
        userDefaults.synchronize()
    }
    
    func getUser()->User?{
        if let data = UserDefaults.standard.data(forKey: "current_user"){
            do {
                return try JSONDecoder().decode(User.self, from: data)
            }catch(let error){
                print(error)
                return nil
            }
        }
        return nil
    }
    
    func deleteUser(){
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "current_user")
        userDefaults.synchronize()
    }
}
