//
//  NotificationsViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 22/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class NotificationsViewController: KanaViewController {
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    private func setupViews(){
        self.title = "Notification"
        self.setupTableView()
    }
    
    private func setupTableView(){
        self.tableView.register(UINib(nibName: "ErrorTableViewCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
    }
}

extension NotificationsViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorCell", for: indexPath)
        return cell
    }
}
