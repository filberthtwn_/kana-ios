//
//  RegisterViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

struct UserInput {
    static var email:String?
    static var password:String?
    static var cPassword:String?
    static var phone:String?
    static var honorofic:String?
    static var firstName:String?
    static var lastName:String?
    static var dateOfBirth:String?
    
    static func reload(){
        self.email = nil
        self.password = nil
        self.cPassword = nil
        self.phone = nil
        self.honorofic = nil
        self.firstName = nil
        self.lastName = nil
        self.lastName = nil
        self.dateOfBirth = nil
    }
}

class FirstRegisterViewController: KanaViewController {
        
    @IBOutlet var emailWarningLabel: UILabel!
    @IBOutlet var passwordWarningLabel: UILabel!
    @IBOutlet var cPasswordWarningLabel: UILabel!
    @IBOutlet var phoneWarningLabel: UILabel!
    
    @IBOutlet var emailTextField: KanaTextField!
    @IBOutlet var passwordTextField: KanaTextField!
    @IBOutlet var cPasswordTextField: KanaTextField!
    @IBOutlet var phoneTextField: KanaTextField!
    
    @IBOutlet var passwordToggleButton: UIButton!
    @IBOutlet var cPasswordToggleButton: UIButton!
    @IBOutlet var registerButton: KanaButton!
    
    private var isEmailValidated = false
    private var isPasswordValidated = false
    private var isPhoneValidated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        
        self.title = "REGISTER"
                
        hideKeyboardWhenTappedAround()
        
        emailTextField.kanaTextFieldDegate = self
        passwordTextField.kanaTextFieldDegate = self
        cPasswordTextField.kanaTextFieldDegate = self
        phoneTextField.kanaTextFieldDegate = self
        
        passwordTextField.removeBorder()
        cPasswordTextField.removeBorder()
    }
    
    private func pushToSecondRegister(){
        if inputValidator(){
            self.navigationController?.pushViewController(SecondRegisterViewController(), animated: true)
        }
    }
    
    @IBAction func passwordToggleAction(_ sender: Any) {
        // Setup Password Toggle
        if passwordTextField.isSecureTextEntry{
            passwordToggleButton.setTitle(String.fontAwesomeIcon(name: .eyeSlash), for: .normal)
        }else{
            passwordToggleButton.setTitle(String.fontAwesomeIcon(name: .eye), for: .normal)
        }
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        // END
    }
    
    @IBAction func cPasswordToggleAction(_ sender: Any) {
        // Setup Password Toggle
        if cPasswordTextField.isSecureTextEntry{
            cPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eyeSlash), for: .normal)
        }else{
            cPasswordToggleButton.setTitle(String.fontAwesomeIcon(name: .eye), for: .normal)
        }
        cPasswordTextField.isSecureTextEntry = !cPasswordTextField.isSecureTextEntry
        // END
    }
    
    @IBAction func registerAction(_ sender: Any) {
        pushToSecondRegister()
    }
}

extension FirstRegisterViewController:UITextFieldDelegate, KanaTextFieldDelegate{
    
    func didValueChanged(sender: UITextField) {
        switch sender {
        case emailTextField:
            if emailTextField.isValidEmail(){
                emailWarningLabel.isHidden = true
                isEmailValidated = true
            }else{
                emailWarningLabel.isHidden = false
                isEmailValidated = false
            }
        case passwordTextField:
            if let passwordInput = passwordTextField.text, passwordInput.count >= 6{
                passwordWarningLabel.isHidden = true
            }else{
                passwordWarningLabel.isHidden = false
            }
            passwordValidator()
        case cPasswordTextField:
            passwordValidator()
        default:
            if let phoneInput = phoneTextField.text, phoneInput.count >= 10, phoneInput.count <= 16 {
                isPhoneValidated = true
                phoneWarningLabel.isHidden = true
            }else{
                phoneWarningLabel.isHidden = false
                isPhoneValidated = false
            }
        }
        _ = inputValidator()
    }
    
    private func passwordValidator(){
        if !cPasswordTextField.text!.isEmpty || cPasswordTextField.isEditing{
            if passwordTextField.text == cPasswordTextField.text{
                isPasswordValidated = true
                cPasswordWarningLabel.isHidden = true
            }else{
                isPasswordValidated = false
                cPasswordWarningLabel.isHidden = false
            }
        }
    }
    
    private func inputValidator()->Bool{
        if isEmailValidated, isPasswordValidated, isPhoneValidated{
            registerButton.isEnabled = true
            registerButton.backgroundColor = .black
            return true
        }else{
            registerButton.isEnabled = false
            registerButton.backgroundColor = Color.PRIMARY_COLOR
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            cPasswordTextField.becomeFirstResponder()
        case cPasswordTextField:
            phoneTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
            pushToSecondRegister()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case emailTextField:
            // MARK: Check Email Input Format
            if emailTextField.isValidEmail(){
                UserInput.email = emailTextField.text
            }
            // END
        case passwordTextField:
            // MARK: Check Password Input Format
            if let passwordInput = passwordTextField.text, passwordInput.count >= 6{
                UserInput.password = passwordTextField.text
            }
            // END
        case cPasswordTextField:
             // MARK: Check Confirm Password
            if passwordTextField.text == cPasswordTextField.text{
                UserInput.cPassword = cPasswordTextField.text
            }
        default:
            if let phoneInput = phoneTextField.text, phoneInput.count >= 10, phoneInput.count <= 16{
                UserInput.phone = phoneTextField.text
            }
        }
    }
}
