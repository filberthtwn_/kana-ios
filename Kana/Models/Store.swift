//
//  Store.swift
//  Kana
//
//  Created by Filbert Hartawan on 08/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import Foundation

struct Store:Decodable {
    var id:String = ""
    var name:String = ""
    var address:String = ""
    var googleAddress:String = ""
    var facility:String = ""
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    var phone:String = ""
    var whatsapp:String = ""
    var workingTime:String = ""
    var imageUrl:String = ""
    
    var gallery:[StoreGallery] = []
    
    enum CodingKeys:String, CodingKey{
        case id = "ID"
        case name = "NAME"
        case nameInNews = "TITLE"
        case address = "ADDRESS"
        case googleAddress = "GOOGLE_ADDRESS"
        case facility = "FASILITAS"
        case latitude = "LATITUDE"
        case longitude = "LOGITUDE"
        case phone = "PHONE"
        case whatsapp = "WHATSAPP"
        case workingTime = "WORK_HOUR"
        case imageUrl = "IMAGE_URL"
        case gallery = "StoreGallery"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let id = try container.decodeIfPresent(String.self, forKey: .id){
            self.id = id
        }
        
        do {
            self.name = try container.decode(String.self, forKey: .name)
        } catch (let error) {
            print(error)
            self.name = try container.decode(String.self, forKey: .nameInNews)
        }
        
        if let address = try container.decodeIfPresent(String.self, forKey: .address){
            self.address = address
        }
        if let googleAddress = try container.decodeIfPresent(String.self, forKey: .googleAddress){
            self.googleAddress = googleAddress
        }
        if let facility = try container.decodeIfPresent(String.self, forKey: .facility){
            self.facility = facility
        }
        if let latitude = try container.decodeIfPresent(String.self, forKey: .latitude){
            self.latitude = Double(latitude)!
        }
        if let longitude = try container.decodeIfPresent(String.self, forKey: .longitude){
            self.longitude = Double(longitude)!
        }
        if let phone = try container.decodeIfPresent(String.self, forKey: .phone){
            self.phone = phone
        }
        if let whatsapp = try container.decodeIfPresent(String.self, forKey: .whatsapp){
            self.whatsapp = whatsapp
        }
        if let workHour = try container.decodeIfPresent(String.self, forKey: .workingTime){
            self.workingTime = workHour
        }
        if let imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl){
            self.imageUrl = imageUrl
        }
        if let gallery = try container.decodeIfPresent([StoreGallery].self, forKey: .gallery){
            self.gallery = gallery
        }
    }
}

struct StoreGallery:Decodable{
    var imageUrl:String = ""
    
    enum CodingKeys:String, CodingKey{
        case imageUrl = "URL_IMAGE"
    }
}

struct StoreHighlight:Codable {
    var title:String?
    var subtitle:String = ""
    var imageUrl: String = ""
    
    // Contact
    var email:String = ""
    var facebook:String = ""
    var instagram:String = ""
    var youtube:String = ""
    var website:String = ""

    
    // Marketplace Link
    var tokopediaLink:String = ""
    var dekorumaLink:String = ""
    
    enum CodingKeys:String, CodingKey {
        case title = "TITLE"
        case subtitle = "SUBTITLE"
        case imageUrl = "IMAGE_COVER"
        
        case email = "EMAIL"
        case facebook = "FACEBOOK"
        case instagram = "INSTAGRAM"
        case youtube = "YOUTUBE"
        case website = "WEBSITE"

        case tokopediaLink = "TOKOPEDIA"
        case dekorumaLink = "DEKORUMA"
    }
}

struct StoreData:Decodable {
    var storeHighlight:StoreHighlight?
    var stores:[Store] = []
    var store:Store?
    var error:String = ""
    
    enum CodingKeys:String, CodingKey {
        case storeHighlight = "HIGHLIGHTSTORE"
        case store = "STORE"
        case error = "ERROR"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            if let storeHighlight = try container.decodeIfPresent(StoreHighlight?.self, forKey: .storeHighlight){
                self.storeHighlight = storeHighlight
            }
        } catch (let error) {
            print(error)
        }
        
        do {
            if let stores = try container.decodeIfPresent([Store].self, forKey: .store){
                self.stores = stores
            }
        } catch (let error) {
            print(error)
            if let store = try container.decodeIfPresent(Store.self, forKey: .store){
                self.store = store
            }
        }
        
        if let error = try container.decodeIfPresent(String.self, forKey: .error){
            self.error = error
        }
    }
}

struct StoreResponse:Decodable{
    var data: StoreData?
    
    enum CodingKeys:String, CodingKey {
        case data = "DATA"
    }
}
