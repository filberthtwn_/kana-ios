//
//  HistoryTransactionTableViewCell.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/09/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit

class HistoryTransactionTableViewCell: UITableViewCell {

    @IBOutlet var salesIdLabel: UILabel!
    @IBOutlet var transferDateLabel: UILabel!
    @IBOutlet var receiptIdLabel: UILabel!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
