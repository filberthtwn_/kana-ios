//
//  LoginViewController.swift
//  Kana
//
//  Created by Filbert Hartawan on 19/08/20.
//  Copyright © 2020 Kana. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SVProgressHUD
import FontAwesome_swift

class LoginViewController: KanaViewController {

    @IBOutlet var passwordToggleButton: KanaButton!
    @IBOutlet var loginButton: KanaButton!
    
    @IBOutlet var passwordView: KanaTextFieldView!
    
    // UITextField
    @IBOutlet var emailTextField: KanaTextField!
    @IBOutlet var passwordTextField: KanaTextField!
    // END
    
    @IBOutlet var emailWarningLabel: UILabel!
    @IBOutlet var passwordWarningLabel: UILabel!
    
    private var isEmailValidated = false
    private var isPasswordValidated = false
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
        self.observeViewModel()
    }
    
    private func setupViews(){
        
        self.title = "LOGIN"
        self.navigationItem.leftBarButtonItem = .none
        
        self.hideKeyboardWhenTappedAround()
        passwordTextField.removeBorder()
        
        emailTextField.kanaTextFieldDegate = self
        passwordTextField.kanaTextFieldDegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UserInput.reload()
    }

    private func observeViewModel(){
        
        self.emailTextField.rx.text.orEmpty
            .bind(to: AuthViewModel.shared.email)
            .disposed(by: disposeBag)
        
        self.passwordTextField.rx.text.orEmpty
        .bind(to: AuthViewModel.shared.password)
        .disposed(by: disposeBag)
        
        self.loginButton.rx.tap.do(onNext:  { [] in
            self.emailTextField.resignFirstResponder()
            self.passwordTextField.resignFirstResponder()
            
        }).subscribe(onNext: { [] in
            SVProgressHUD.show()
            AuthViewModel.shared.login()
        }).disposed(by: disposeBag)
        
        AuthViewModel.shared.isLoginSuccess.bind { (isSuccess) in
            if isSuccess{
                self.disposeBag = DisposeBag()
                SVProgressHUD.dismiss()
                self.moveToMainTabBar(animated: true)
            }
        }.disposed(by: disposeBag)
        
        AuthViewModel.shared.errorMsg.asObservable().bind { (errorMsg) in
            guard let errorMsg = errorMsg else {return}
            SVProgressHUD.showError(withStatus: errorMsg)
            SVProgressHUD.dismiss(withDelay: Delay.SHORT)
        }.disposed(by: disposeBag)
    }
    
    private func moveToMainTabBar(animated:Bool){
        let navController = UINavigationController.init(rootViewController: TabBarViewController())
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: animated, completion: nil)
    }

    @IBAction func passwordToggleAction(_ sender: Any) {
        // Setup Password Toggle
        if passwordTextField.isSecureTextEntry{
            passwordToggleButton.setTitle(String.fontAwesomeIcon(name: .eyeSlash), for: .normal)
        }else{
            passwordToggleButton.setTitle(String.fontAwesomeIcon(name: .eye), for: .normal)
        }
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        // END
    }
    
    @IBAction func skipAction(_ sender: Any) {
        self.moveToMainTabBar(animated: true)
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        self.navigationController?.pushViewController(ForgotPasswordViewController(), animated: true)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        self.navigationController?.pushViewController(FirstRegisterViewController(), animated: true)
    }
}

extension LoginViewController:UITextFieldDelegate, KanaTextFieldDelegate{
    
    func didValueChanged(sender: UITextField) {
        switch sender {
        case emailTextField:
            if emailTextField.isValidEmail(){
                emailWarningLabel.isHidden = true
                isEmailValidated = true
            }else{
                emailWarningLabel.isHidden = false
                isEmailValidated = false
            }
        case passwordTextField:
            if passwordTextField.text!.count >= 6{
                passwordWarningLabel.isHidden = true
                isPasswordValidated = true
            }else{  
                passwordWarningLabel.isHidden = false
                isPasswordValidated = false
            }
        default:
            break
        }
        inputValidator()
    }
    
    private func inputValidator(){
        if isEmailValidated, isPasswordValidated{
            loginButton.isEnabled = true
            loginButton.backgroundColor = .black
        }else{
            loginButton.isEnabled = false
            loginButton.backgroundColor = Color.PRIMARY_COLOR
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField{
            passwordTextField.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}
